﻿using System.ComponentModel.DataAnnotations;
using Database.Models;

namespace Admin.ViewModels
{
    public class EditMovieModel : MovieModel
    {
        [Required]
        public int Id { get; set; }

        public EditMovieModel() { }

        public EditMovieModel(Movie movie) : base(movie)
        {
            Id = movie.MovieId;
        }
    }
}