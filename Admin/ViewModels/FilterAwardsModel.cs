﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Admin.ViewModels
{
    public class FilterAwardsModel
    {
        [DisplayName("From")]
        public int? FromYear { get; set; }

        [DisplayName("Year")]
        public int? TillYear { get; set; }

        public int? Page { get; set; }
    }
}
