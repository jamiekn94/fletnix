﻿using System.ComponentModel.DataAnnotations;
using Database.Models;

namespace Admin.ViewModels
{
    public class SearchCastMemberModel
    {
        public SearchCastMemberModel() { }

        public SearchCastMemberModel(int movieId, string viewName)
        {
            MovieId = movieId;
            ViewName = viewName;
        }

        [Required]
        public int MovieId { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "The name must contain 3 characters.")]
        public string Name { get; set; }

        [Required]
        public string ViewName { get; set; }
    }
}
