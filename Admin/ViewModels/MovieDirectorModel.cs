﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Admin.ViewModels
{
    public class MovieDirectorModel
    {
        public MovieDirectorModel() { }

        public MovieDirectorModel(int movieId, string searchMember = null)
        {
            MovieId = movieId;
            SearchMember = searchMember;
        }

        [Required]
        public string SearchMember { get; set; }

        [Required]
        [DisplayName("Person")]
        public int PersonId { get; set; }

        [Required]
        public int MovieId { get; set; }
    }
}