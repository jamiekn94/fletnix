﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Database.Models;

namespace Admin.ViewModels
{
    public class MovieCastModel
    {
        public MovieCastModel() { }

        public MovieCastModel(MovieCast cast)
        {
            MovieId = cast.MovieId;
            PersonId = cast.PersonId;
            Role = cast.Role;
        }

        public MovieCastModel(int movieId, string searchMember = null)
        {
            MovieId = movieId;
            SearchMember = searchMember;
        }

        [Required]
        public int MovieId { get; set; }

        [Required]
        [DisplayName("Person")]
        public int PersonId { get; set; }

        [Required]
        public string Role { get; set; }
        
        public string SearchMember { get; set; }
    }
}
