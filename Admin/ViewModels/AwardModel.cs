﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Admin.ViewModels
{
    public class AwardModel
    {
        public AwardModel() { }

        public AwardModel(int personId)
        {
            PersonId = personId;
        }

        public AwardModel(SearchAwardModel searchModel)
        {
            MovieId = searchModel.MovieId;
            PersonId = searchModel.PersonId;
            Name = searchModel.Name;
        }

        public int MovieId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int? Year { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        [DisplayName("Person")]
        public int PersonId { get; set; }

        [Required]
        public string Result { get; set; }
    }
}
