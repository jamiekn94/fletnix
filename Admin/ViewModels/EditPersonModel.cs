﻿using System.ComponentModel.DataAnnotations;
using Database.Models;

namespace Admin.ViewModels
{
    public class EditPersonModel : PersonModel
    {
        public EditPersonModel() { }

        public EditPersonModel(Person person)
        {
            Id = person.PersonId;
            FirstName = person.Firstname;
            LastName = person.Lastname;
            Gender = person.Gender;
        }

        [Required]
        public int Id { get; set; }
    }
}
