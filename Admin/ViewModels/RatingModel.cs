﻿namespace Admin.ViewModels
{
    public enum Order
    {
        Ascending = 0,
        Descending = 1,
        RatingPrice = 2
    }

    public class RatingModel
    {
        public RatingModel()
        {
            Order = Order.Descending;
        }

        public int Page { get; set; }

        public Order Order { get; set; }
    }
}
