﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Admin.ViewModels
{
    public class SearchAwardModel
    {
        public SearchAwardModel() { }

        public SearchAwardModel(int movieId)
        {
            MovieId = movieId;
        }

        [Required]
        public int MovieId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DisplayName("Person")]
        public int PersonId { get; set; }
    }
}
