﻿using System.ComponentModel.DataAnnotations;

namespace Admin.ViewModels
{
    public class EditAwardModel : AwardModel
    {
        public EditAwardModel() { }

        public EditAwardModel(EditSearchAwardModel model)
        {
            Id = model.Id;
            MovieId = model.MovieId;
            Name = model.Name;
            PersonId = model.PersonId;
            Year = model.Year;
        }

        [Required]
        public int Id { get; set; }
    }
}
