﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Admin.ViewModels
{
    public class PersonModel
    {
        [Required]
        [DisplayName("First name")]
        public string FirstName { get; set; }

        [Required]
        [DisplayName("Last name")]
        public string LastName { get; set; }

        [Required]
        public string Gender { get; set; }
    }
}
