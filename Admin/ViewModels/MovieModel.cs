﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Database.Models;
using Microsoft.AspNetCore.Http;

namespace Admin.ViewModels
{
    public class MovieModel
    {
        public MovieModel() { }

        public MovieModel(Movie movie)
        {
            Title = movie.Title;
            Description = movie.Description;
            Duration = movie.Duration;
            Genres = movie.MovieGenre.Select(g => g.GenreName).ToList();
            Price = movie.Price;
            Url = movie.Url;
            PublicationYear = movie.PublicationYear;
        }

        [Required]
        public string Title { get; set; }

        [Required]
        public int? Duration { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [DisplayName("Publication year")]
        public int? PublicationYear { get; set; }

        [DataType(DataType.Upload)]
        [DisplayName("Image")]
        public IFormFile CoverImage { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        [Url]
        public string Url { get; set; }

        [Required]
        public List<string> Genres { get; set; }
    }
}