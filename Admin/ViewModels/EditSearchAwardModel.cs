﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Database.Models;

namespace Admin.ViewModels
{
    public class EditSearchAwardModel : SearchAwardModel
    {
        public EditSearchAwardModel() { }

        public EditSearchAwardModel(MovieAward award)
        {
            Id = award.Id;
            MovieId = award.MovieId;
            Name = award.Name;
            PersonId = award.PersonId;
            Year = award.Year;
        }

        [Required]
        public int Id { get; set; }

        [Required]
        public int Year { get; set; }
    }
}
