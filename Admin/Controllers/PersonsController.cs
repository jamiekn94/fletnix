using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin.ViewModels;
using Database;
using Database.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Admin.Controllers
{
    [Authorize(Policy = "Administrator")]
    public class PersonsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public PersonsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> Index(int page = 0)
        {
            const int limit = 20;

            var taskPersons = _unitOfWork.PersonRepository.GetAll(page, limit);
            var countPersons = _unitOfWork.PersonRepository.Count();

            ViewData["persons"] = (await taskPersons).Select(x => new Person
            {
                PersonId = x.PersonId,
                Firstname = x.Firstname,
                Lastname = x.Lastname,
                Gender = GetGenderByCharacter(x.Gender)
            }).ToList();
            ViewData["amount"] = await countPersons;
            ViewData["hasNextPage"] = await countPersons > 0 * limit;
            ViewData["hasPreviousPage"] = page > 0;
            ViewData["page"] = page;

            return View();
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _unitOfWork.PersonRepository.Remove(id);

            return RedirectToAction("Index");
        }

        public IActionResult Create()
        {
            ViewData["genders"] = GetGenders();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PersonModel model)
        {
            if (ModelState.IsValid)
            {
                await _unitOfWork.PersonRepository.Add(model.FirstName, model.LastName, model.Gender);

                return RedirectToAction("Index");
            }

            ViewData["genders"] = GetGenders();

            return View(model);

        }

        public async Task<IActionResult> Edit(int id)
        {
            var person = await _unitOfWork.PersonRepository.GetSingle(id);

            if (person == null)
            {
                return NotFound();
            }

            ViewData["genders"] = GetGenders();

            return View(new EditPersonModel(person));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditPersonModel model)
        {
            if (ModelState.IsValid)
            {
                await _unitOfWork.PersonRepository.Edit(model.Id, model.FirstName, model.LastName, model.Gender);

                return RedirectToAction("Index");
            }

            ViewData["genders"] = GetGenders();

            return View(model);
        }

        private List<SelectListItem> GetGenders()
        {
            return new List<SelectListItem>
            {
                new SelectListItem
                {
                    Value = "M",
                    Text = "Male"
                },
                new SelectListItem{
                    Value = "F",
                    Text = "Female"
                }
            };
        }

        private string GetGenderByCharacter(string character)
        {
            var gender = GetGenders().FirstOrDefault(x => x.Value == character);

            return gender?.Text ?? "Unknown";
        }
    }
}