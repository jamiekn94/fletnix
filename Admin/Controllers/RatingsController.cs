using System.Collections.Generic;
using System.Threading.Tasks;
using Admin.ViewModels;
using Database;
using Database.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Admin.Controllers
{
    [Authorize(Policy = "Administrator")]
    public class RatingsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public RatingsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> Index(RatingModel model)
        {
            const int limit = 10;

            var taskMovies = GetMoviesByOrder(model.Order, model.Page, limit);
            var taskCountMovies = CountMovies(model.Order);

            ViewData["movies"] = await taskMovies;
            ViewData["amount"] = await taskCountMovies;
            ViewData["page"] = model.Page;
            ViewData["hasNextPage"] = await taskCountMovies > 0 * limit;
            ViewData["hasPreviousPage"] = false;
            ViewData["options"] = GetOptions();

            return View(model);
        }

        private async Task<List<Movie>> GetMoviesByOrder(Order order, int page, int limit)
        {
            switch (order)
            {
                case Order.Ascending:
                    {
                        return await _unitOfWork.MovieRepository.GetMostUnPopularMoviesEver(page, limit);
                    }
                case Order.Descending:
                    {
                        return await _unitOfWork.MovieRepository.GetMostPopularMoviesEver(page, limit);
                    }
                default:
                    {
                        return await _unitOfWork.MovieRepository.GetMoviesOrderedByRatingPrice(page, limit);
                    }
            }
        }

        private async Task<int> CountMovies(Order order)
        {
            switch (order)
            {
                case Order.Descending:
                case Order.Ascending:
                    {
                        return await _unitOfWork.MovieRepository.CountMovies();
                    }
                default:
                    {
                        return await _unitOfWork.MovieRepository.CountMoviesWithFeedBack();
                    }
            }
        }

        private List<SelectListItem> GetOptions()
        {
            return new List<SelectListItem>
            {
                new SelectListItem
                {
                    Value = ((int) Order.Descending).ToString(),
                    Text = "Hoogste rating"
                },
                new SelectListItem
                {
                    Value = ((int) Order.Ascending).ToString(),
                    Text = "Laagste rating"
                },
                new SelectListItem
                {
                    Value = ((int) Order.RatingPrice).ToString(),
                    Text = "Hoogste rating/price"
                }
            };
        }
    }
}