using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Admin.ViewModels;
using Database;
using Database.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Admin.Controllers
{
    [Authorize(Policy = "Administrator")]
    public class MoviesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public MoviesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> Index(int page = 0)
        {
            const int limit = 25;
            ViewData["page"] = page;
            return View(await _unitOfWork.MovieRepository.GetPage(page, limit));
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _unitOfWork.MovieRepository.GetSingle(id.Value);

            if (movie == null)
            {
                return NotFound();
            }

            return View(movie);
        }

        public async Task<IActionResult> Create()
        {
            ViewData["genres"] = await _unitOfWork.GenreRepository.GetAll();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MovieModel model)
        {
            if (ModelState.IsValid)
            {
                var movie = new Movie
                {
                    Title = model.Title,
                    Duration = model.Duration,
                    Description = model.Description,
                    PublicationYear = model.PublicationYear,
                    CoverImage = await GetByteArrayFromFile(model.CoverImage),
                    Price = model.Price,
                    MovieGenre = model.Genres.Select(g => new MovieGenre { GenreName = g }).ToList(),
                    Url = model.Url
                };

                await _unitOfWork.MovieRepository.Add(movie);

                return RedirectToAction("Details", new {id = movie.MovieId});
            }

            ViewData["genres"] = await _unitOfWork.GenreRepository.GetAll();

            return View(model);
        }

        public async Task<IActionResult> EditCast(int id, int personId, string role)
        {
            var movie = await _unitOfWork.MovieRepository.GetSingle(id);

            if (movie == null)
            {
                return NotFound();
            }

            var person = movie.MovieCast.FirstOrDefault(c => c.PersonId == personId && c.Role == role);

            if (person == null)
            {
                return NotFound();
            }

            return View(new MovieCastModel(person));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditCast(MovieCastModel model)
        {
            if (ModelState.IsValid)
            {
                await _unitOfWork.MovieRepository.UpdateCast(model.MovieId, model.PersonId, model.Role);

                return RedirectToAction("Details", new { id = model.MovieId });
            }

            return View(model);
        }

        public IActionResult SearchCastMember(int id, string viewName)
        {
            return View(new SearchCastMemberModel(id, viewName));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchCastMember(SearchCastMemberModel model)
        {
            if (ModelState.IsValid)
            {
                var persons = await _unitOfWork.PersonRepository.Find(model.Name);

                ViewData["persons"] = persons;

                return View(model.ViewName, GetModel(model));

                //return View(model.ViewName, model.ViewName == "AddCast" ? (object) new MovieCastModel(model.MovieId, model.Name) : new MovieDirectorModel(model.MovieId, model.Name));
            }

            return View(model);
        }

        public IActionResult AddCast(int id)
        {
            return View(new MovieCastModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddCast(MovieCastModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Why the hell did i create a new repository for this?
                    // todo: Just add the movie cast method in the movie repository
                    await _unitOfWork.MovieCastRepository.Add(new MovieCast
                    {
                        MovieId = model.MovieId,
                        PersonId = model.PersonId,
                        Role = model.Role
                    });

                    return RedirectToAction("Details", new { id = model.MovieId });
                }
                catch (DbUpdateException)
                {
                    ModelState.AddModelError(string.Empty, "Cast members must be unique.");
                }
            }

            var persons = await _unitOfWork.PersonRepository.Find(model.SearchMember);

            ViewData["persons"] = persons;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddAward(AwardModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _unitOfWork.MovieRepository.AddAward(model.MovieId, model.Name, model.Type, model.PersonId,
                        model.Result, model.Year.Value);

                    return RedirectToAction("Details", new { id = model.MovieId });
                }
                catch (DbUpdateException)
                {
                    ModelState.AddModelError(string.Empty, "Award must be unique.");
                }
            }

            ViewData["types"] = await _unitOfWork.AwardRepository.GetAwardTypes(model.Name);
            ViewData["results"] = GetAwardResults();

            return View(model);
        }

        public async Task<IActionResult> SearchAward(int id)
        {
            var awards = _unitOfWork.AwardRepository.GetAll();
            var persons = _unitOfWork.MovieRepository.GetCast(id);

            ViewData["awards"] = await awards;
            ViewData["persons"] = await persons;

            return View(new SearchAwardModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchAward(SearchAwardModel model)
        {
            if (ModelState.IsValid)
            {
                var types = _unitOfWork.AwardRepository.GetAwardTypes(model.Name);
                var persons = _unitOfWork.MovieRepository.GetCast(model.MovieId);

                ViewData["results"] = GetAwardResults();
                ViewData["types"] = await types;
                ViewData["persons"] = persons;

                return View("AddAward", new AwardModel(model));
            }

            ViewData["awards"] = await _unitOfWork.AwardRepository.GetAll();

            return View(model);
        }

        public async Task<IActionResult> EditSearchAward(int id)
        {
            var award = await _unitOfWork.MovieRepository.GetAward(id);
            var awards = _unitOfWork.AwardRepository.GetAll();
            var persons = _unitOfWork.MovieRepository.GetCast(award.MovieId);

            ViewData["awards"] = await awards;
            ViewData["persons"] = await persons;

            return View(new EditSearchAwardModel(award));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditSearchAward(EditSearchAwardModel model)
        {
            if (ModelState.IsValid)
            {
                var types = _unitOfWork.AwardRepository.GetAwardTypes(model.Name);
                var persons = _unitOfWork.MovieRepository.GetCast(model.MovieId);

                ViewData["results"] = GetAwardResults();
                ViewData["types"] = await types;
                ViewData["persons"] = await persons;

                return View("EditAward", new EditAwardModel(model));
            }

            ViewData["awards"] = await _unitOfWork.AwardRepository.GetAll();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAward(EditAwardModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _unitOfWork.MovieRepository.EditAward(model.Id, model.Name, model.Type, model.PersonId,
                        model.Result, model.Year.Value);

                    return RedirectToAction("Details", new { id = model.MovieId });
                }
                catch (DbUpdateException)
                {
                    ModelState.AddModelError(string.Empty, "Award must be unique.");
                }
            }

            var types = _unitOfWork.AwardRepository.GetAwardTypes(model.Name);
            var persons = _unitOfWork.MovieRepository.GetCast(model.MovieId);

            ViewData["results"] = GetAwardResults();
            ViewData["types"] = await types;
            ViewData["persons"] = await persons;

            return View(model);
        }

        public IActionResult AddDirector(int id)
        {
            return View(new MovieDirectorModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddDirector(MovieDirectorModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _unitOfWork.MovieRepository.AddDirector(model.MovieId, model.PersonId);

                    return RedirectToAction("Details", new { id = model.MovieId });
                }
                catch (DbUpdateException)
                {
                    ModelState.AddModelError(string.Empty, "Directors must be unique.");
                }
            }

            var persons = await _unitOfWork.PersonRepository.Find(model.SearchMember);

            ViewData["persons"] = persons;

            return View(model);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _unitOfWork.MovieRepository.GetSingle(id.Value);

            if (movie == null)
            {
                return NotFound();
            }

            ViewData["genres"] = await _unitOfWork.GenreRepository.GetAll();

            return View(new EditMovieModel(movie));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditMovieModel model)
        {
            if (ModelState.IsValid)
            {
                await _unitOfWork.MovieRepository.Update(model.Id, model.Title, model.Duration, model.Description,
                    model.PublicationYear, await GetByteArrayFromFile(model.CoverImage), model.Price, model.Url,
                    model.Genres);

                return RedirectToAction("Details", new { id = model.Id });
            }

            ViewData["genres"] = await _unitOfWork.GenreRepository.GetAll();

            return View(model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _unitOfWork.MovieRepository.Delete(id);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteCast(int id, int personId, string role)
        {
            await _unitOfWork.MovieRepository.DeletePersonFromCast(id, personId, role);

            return RedirectToAction("Details", new { id });
        }

        public async Task<IActionResult> DeleteGenre(int id, string genre)
        {
            await _unitOfWork.MovieRepository.DeleteGenre(id, genre);

            return RedirectToAction("Details", new { id });
        }

        public async Task<IActionResult> DeleteDirector(int id, int personId)
        {
            await _unitOfWork.MovieRepository.DeleteDirector(id, personId);

            return RedirectToAction("Details", new { id });
        }

        public async Task<IActionResult> DeleteAward(int id, int personId, string name, string type, int year)
        {
            await _unitOfWork.MovieRepository.DeleteAward(id, personId, name, type, year);

            return RedirectToAction("Details", new { id });
        }

        private async Task<byte[]> GetByteArrayFromFile(IFormFile file)
        {
            if (file == null)
            {
                return null;
            }

            using (var memoryStream = new MemoryStream())
            {
                // This compiles just fine, what the hell happened to visual studio 2017?
                await file.CopyToAsync(memoryStream);

                return memoryStream.ToArray();
            }
        }

        private object GetModel(SearchCastMemberModel searchCastMember, object extraModel = null)
        {
            switch (searchCastMember.ViewName)
            {
                case "AddCast":
                    {
                        return new MovieCastModel(searchCastMember.MovieId, searchCastMember.Name);
                    }
                case "AddDirector":
                    {
                        return new MovieDirectorModel(searchCastMember.MovieId, searchCastMember.Name);
                    }
            }

            throw new ArgumentException($"Unknown view name: {searchCastMember.ViewName}");
        }

        private List<string> GetAwardResults()
        {
            return new List<string>
            {
                "Won",
                "Nominated"
            };
        }
    }
}