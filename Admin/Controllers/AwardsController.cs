using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin.ViewModels;
using Database;
using Database.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin.Controllers
{
    [Authorize(Policy = "Administrator")]
    public class AwardsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private const int LIMIT = 20;

        public AwardsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> Index(int page = 0)
        {
            var movies = _unitOfWork.MovieRepository.GetMoviesGroupedByYear(page, LIMIT);
            var amountMovies = _unitOfWork.MovieRepository.CountMoviesGroupedByYear();

            ViewData["movies"] = await movies;
            ViewData["amountMovies"] = await amountMovies;
            ViewData["hasPreviousPage"] = HasNextPage(page);
            ViewData["hasNextPage"] = HasNextPage(page, await amountMovies);
            ViewData["page"] = page;

            return View();
        }

        public async Task<IActionResult> Search(FilterAwardsModel model)
        {
            if (ModelState.IsValid)
            {
                int page = model.Page ?? 0;
                var movies = _unitOfWork.MovieRepository.GetMoviesGroupedByYear(model.FromYear, model.TillYear, page, LIMIT);
                var amountMovies = _unitOfWork.MovieRepository.CountMoviesGroupedByYear(model.FromYear, model.TillYear);

                ViewData["movies"] = await movies;
                ViewData["amountMovies"] = await amountMovies;
                ViewData["hasPreviousPage"] = HasNextPage(page);
                ViewData["hasNextPage"] = HasNextPage(page, await amountMovies);
                ViewData["page"] = page;

                return View("Index");
            }

            ViewData["movies"] = new List<Movie>();
            ViewData["amountMovies"] = 0;
            ViewData["hasPreviousPage"] = false;
            ViewData["hasNextPage"] = false;
            ViewData["page"] = 0;

            return View("Index");
        }

        private bool HasNextPage(int page)
        {
            return page > 0;
        }

        private bool HasNextPage(int page, int amountMovies)
        {
            return amountMovies > (page + 1) * LIMIT;
        }
    }
}