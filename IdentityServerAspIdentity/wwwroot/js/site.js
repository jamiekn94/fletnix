﻿var app = angular.module('Fletnix', ['ngRoute'])
    .config(function ($routeProvider, $locationProvider) {

        $locationProvider.html5Mode({
            enabled: true,
            rewriteLinks: false
        });

        $routeProvider.when('/Movies',
            {
                templateUrl: '/templates/movies.html',
                controller: 'MoviesController'
            }).when('/movies/:id',
            {
                templateUrl: '/templates/movie.html',
                controller: 'movieController'
            });

    }).run(function() {
        console.log('Running kiddo!');
    });