﻿angular.module("Fletnix").service("userService",
    function () {

        var config = {
            authority: "http://localhost:5000",
            client_id: "js",
            redirect_uri: "http://localhost:5000/Account/Callback",
            response_type: "token",
            scope: "api1",
            post_logout_redirect_uri: "http://localhost:5000"
        };

        var mgr = new Oidc.UserManager(config);

        return mgr;
    });