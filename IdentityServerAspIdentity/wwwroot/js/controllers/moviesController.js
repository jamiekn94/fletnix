﻿angular.module("Fletnix").controller('MoviesController',
    function($scope, userService) {
        console.log('movies!');

        userService.getUser().then(function (user) {
            if (user) {
                console.log("User logged in", user.profile);
            }
            else {
                userService.signinRedirect();
                console.log("User not logged in");
            }
        });
    });