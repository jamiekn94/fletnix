﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Identity.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        public RegisterViewModel(IEnumerable<string> countries)
        {
            Countries = countries;
        }

        public RegisterViewModel() { }

        [Required]
        [StringLength(25, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "The country can only be {0} characters long.")]
        [Display(Name = "Country")]
        public string CountryName { get; set; }

        [Required]
        [Display(Name = "Paypal")]
        [MaxLength(250, ErrorMessage = "The paypal account can only be {0} characters long.")]
        public string PaypalAccount { get; set; }

        public IEnumerable<string> Countries { get; set; }
    }
}
