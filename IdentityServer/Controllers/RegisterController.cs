﻿using System.Linq;
using System.Threading.Tasks;
using Database.Models;
using IdentityServer.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FletNix.Controllers
{
    public class RegisterController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> loginManager;
        private readonly RoleManager<ApplicationRole> roleManager;

        private const string Role = "Customer";


        public RegisterController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> loginManager,
            RoleManager<ApplicationRole> roleManager)
        {
            this.userManager = userManager;
            this.loginManager = loginManager;
            this.roleManager = roleManager;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser
                {
                    Email = model.Email,
                    UserName = model.Email
                };

                var identityResult = await userManager.CreateAsync
                    (user, model.Password);

                if (identityResult.Succeeded)
                {
                    await CreateUserRole();

                    await userManager.AddToRoleAsync(user,
                        Role);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    identityResult.Errors.ToList().ForEach(error =>
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    });
                }
            }

            return View("Index", model);
        }

        private async Task CreateUserRole()
        {
            bool roleExists = await roleManager.RoleExistsAsync(Role);

            if (!roleExists)
            {
                ApplicationRole role = new ApplicationRole
                {
                    Name = Role,
                    Description = "Kijkt films."
                };

                await roleManager.
                    CreateAsync(role);
            }
        }
    }
}