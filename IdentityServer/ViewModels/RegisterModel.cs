﻿using System.ComponentModel.DataAnnotations;

namespace IdentityServer.ViewModels
{
    public class RegisterModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
