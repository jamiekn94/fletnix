﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Interfaces;
using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class AwardRepository : Repository<Country, string>, IAwardRepository
    {
        public AwardRepository(FletnixContext fletnixContext)  : base(fletnixContext) { }

        public async Task<List<string>> GetAll()
        {
            return await Context.Award.Select(dbSet => dbSet.Name).AsNoTracking().ToListAsync();
        }

        public async Task<List<string>> GetAwardTypes(string name)
        {
            return await Context.AwardType.Where(t => t.Name == name).Select(t => t.Type).ToListAsync();
        }
    }
}
