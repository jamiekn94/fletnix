﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Database.Interfaces;
using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class MovieCastRepository : Repository<MovieCast, string>, IMovieCastRepository
    {
        public MovieCastRepository(FletnixContext fletnixContext) : base(fletnixContext) { }

        public async Task<List<MovieCast>> GetAll()
        {
            return await Context.MovieCast.AsNoTracking().ToListAsync();
        }
    }
}
