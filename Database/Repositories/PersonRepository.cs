﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Interfaces;
using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class PersonRepository : Repository<Person, int>, IPersonRepository
    {
        public PersonRepository(FletnixContext fletnixContext) : base(fletnixContext) { }

        public async Task<List<Person>> GetAll()
        {
            return await Context.Person.AsNoTracking().OrderBy(x => x.Firstname).ThenBy(x => x.Lastname).ToListAsync();
        }

        public async Task<List<Person>> GetAll(int page, int limit)
        {
            return await Context.Person.AsNoTracking().OrderBy(x => x.Firstname).ThenBy(x => x.Lastname).Skip(page * limit).Take(limit).ToListAsync();
        }

        public async Task<List<Person>> Find(string name)
        {
            return await Context.Person.Where(p => (p.Firstname + " " + p.Lastname).Contains(name)).AsNoTracking().ToListAsync();
        }

        public async Task Add(string firstName, string lastName, string gender)
        {
            var person = new Person
            {
                Firstname = firstName,
                Lastname = lastName,
                Gender = gender
            };

            Context.Person.Add(person);

            await Context.SaveChangesAsync();
        }

        public Task<int> Count()
        {
            return Context.Person.CountAsync();
        }

        public async Task Edit(int id, string firstName, string lastName, string gender)
        {
            var person = Context.Person.Find(id);

            if (person != null)
            {
                person.Firstname = firstName;
                person.Lastname = lastName;
                person.Gender = gender;
            }

            await Context.SaveChangesAsync();
        }

        public async Task Remove(int id)
        {
            var person = Context.Person.Find(id);

            if (person != null)
            {
                Context.Person.Remove(person);
                await Context.SaveChangesAsync();
            }
        }
    }
}
