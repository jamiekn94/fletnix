﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Interfaces;
using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class WatchHistoryRepository : Repository<Country, string>, IWatchHistoryRepository
    {
        public WatchHistoryRepository(FletnixContext fletnixContext)  : base(fletnixContext) { }

        public async Task Add(string userId, Movie movie, DateTime date)
        {
            Context.WatchHistory.Add(new WatchHistory
            {
                MovieId = movie.MovieId,
                UserId = userId,
                WatchDate = date,
                Price = movie.Price
            });

            await Context.SaveChangesAsync();
        }

        public async Task<List<WatchHistory>> GetByUserId(string userId, int page, int limit)
        {
            return await Context.WatchHistory.Include(x => x.Movie).Where(m => m.UserId == userId).Skip(page * limit).Take(limit).ToListAsync();
        }

        public async Task<int> CountByUserId(string userId)
        {
            return await Context.WatchHistory.CountAsync(m => m.UserId == userId);
        }

        public async Task<bool> HasWatched(string userId, int movieId)
        {
            return await Context.WatchHistory.AnyAsync(m => m.UserId == userId && m.MovieId == movieId);
        }
    }
}
