﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.Interfaces;
using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class MovieRepository : Repository<Movie, int>, IMovieRepository
    {
        public MovieRepository(FletnixContext fletnixContext) : base(fletnixContext)
        {
        }

        public override async Task Delete(int id)
        {
            var item = await GetSingle(id);

            if (item != null)
            {
                var relatedMovies = Context.Movie.Where(x => x.PreviousPart == id).ToListAsync();

                (await relatedMovies).ForEach(relatedMovie =>
                {
                    relatedMovie.PreviousPart = null;
                });

                await Context.SaveChangesAsync();

                Context.Movie.Remove(item);

                await Context.SaveChangesAsync();
            }
        }

        public async Task<List<Movie>> GetPage(int skip, int limit)
        {
            return await Context.Movie.Skip(skip).Take(limit).AsNoTracking().ToListAsync();
        }

        public async Task<List<string>> GetNames()
        {
            return await Context.Movie.Select(m => m.Title).ToListAsync();
        }

        public async Task<List<Movie>> GetAll(List<string> genres, int? year, decimal? minPrice, decimal? maxPrice,
            int page,
            int limit)
        {
            IQueryable<Movie> query = Context.Movie.AsQueryable();

            return await WhereFilter(year, minPrice, maxPrice, query)
                .OrderBy(m => m.Title)
                .Skip(page * limit)
                .Take(limit)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> CountMovies()
        {
            return await Task.Run(() => Context.Movie.Count());
        }

        public async Task<List<Movie>> GetMostPopularMoviesOfLastTwoWeeks(List<string> genres, int? year,
            decimal? minPrice,
            decimal? maxPrice, int page, int limit)
        {
            DateTime date = DateTime.Now.AddDays(-14);

            return
                await Context.Movie.FromSql(
                        "SELECT [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL], AVG(rating) AS average " +
                        "FROM [Movie] AS [m] " +
                        "LEFT JOIN [UserFeedback] AS [c] ON [m].[movie_id] = [c].[MovieId] " +
                        $"WHERE [c].[Date] >= '{date}' " +
                        GetWhereQuery(year, minPrice, maxPrice, false) +
                        "GROUP By [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL]" +
                        "ORDER BY average DESC " +
                        GetOffsetQuery(page, limit))
                    .ToListAsync();

            /* return
                 await query
                 .AsNoTracking()
                 .Join(Context.Movie, movie => movie.MovieId, feedback => feedback.MovieId, (feedback, movie) => movie)
                 .GroupBy(x => x.MovieId)
                 .Select(x => x.First())
                 .GroupJoin(Context.WatchHistory, movie => movie.MovieId, watchHistory => watchHistory.MovieId, (movie, watchHistory) => new { movie, watchHistory })
                 .OrderByDescending(x => x.watchHistory.Count())
                 .Select(x => x.movie)
                 .Skip(page * limit)
                 .Take(limit)
                 .ToListAsync();*/
        }

        public async Task<List<Movie>> GetMostPopularMoviesEver(List<string> genres, int? year, decimal? minPrice,
            decimal? maxPrice,
            int page, int limit)
        {
            return
                await Context.Movie.FromSql(
                        "SELECT [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL], AVG(rating) AS average " +
                        "FROM [Movie] AS [m] " +
                        "LEFT JOIN [UserFeedback] AS [c] ON [m].[movie_id] = [c].[MovieId] " +
                        GetWhereQuery(year, minPrice, maxPrice, true) +
                        "GROUP By [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL]" +
                        "ORDER BY average DESC " +
                        GetOffsetQuery(page, limit))
                    .ToListAsync();
        }

        public async Task<List<Movie>> GetMostPopularMoviesEver(int page, int limit)
        {
            return
                await Context.Movie.FromSql(
                        "SELECT [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL], AVG(rating) AS average " +
                        "FROM [Movie] AS [m] " +
                        "LEFT JOIN [UserFeedback] AS [c] ON [m].[movie_id] = [c].[MovieId] " +
                        "GROUP By [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL]" +
                        "ORDER BY average DESC " +
                        GetOffsetQuery(page, limit))
                    .ToListAsync();
        }

        public async Task<int> CountMovies(List<string> genres, int? year, decimal? minPrice, decimal? maxPrice)
        {
            var query = Context.Movie.AsQueryable();

            query = WhereFilter(year, minPrice, maxPrice, query);

            return await query.OrderByDescending(t => t.WatchHistory.Count)
                .CountAsync();
        }

        public async Task<int> CountMostPopularMoviesOfLastTwoWeeksList(List<string> genres, int? year,
            decimal? minPrice, decimal? maxPrice)
        {
            DateTime date = DateTime.Now.AddDays(-14);

            var query = Context.UserFeedback.Where(x => x.Date >= date)
                .Join(Context.Movie, movie => movie.MovieId, feedback => feedback.MovieId, (feedback, movie) => movie);

            if (year.HasValue)
            {
                query = query.Where(q => q.PublicationYear == year);
            }

            if (minPrice.HasValue)
            {
                query = query.Where(q => q.Price >= minPrice);
            }

            if (maxPrice.HasValue)
            {
                query = query.Where(q => q.Price <= maxPrice);
            }

            return await query.CountAsync();
        }

        public async Task Update(int id, string title, int? duration, string description, int? publicationYear,
            byte[] coverImage, decimal price, string url, List<string> genres)
        {
            var movie = await GetSingle(id);

            if (movie == null)
            {
                return;
            }

            movie.Title = title;
            movie.Duration = duration;
            movie.Description = description;
            movie.PublicationYear = publicationYear;

            if (coverImage != null)
            {
                movie.CoverImage = coverImage;
            }

            movie.Price = price;
            movie.Url = url;

            RemoveGenres(genres, movie);

            AddNewGenres(genres, movie);

            Context.Movie.Update(movie);

            await Context.SaveChangesAsync();
        }

        public async Task UpdateCast(int movieId, int personId, string role)
        {
            var movie = await GetSingle(movieId);

            if (movie == null)
            {
                return;
            }

            var person = movie.MovieCast.FirstOrDefault(c => c.PersonId == personId);

            if (person == null)
            {
                return;
            }

            person.Role = role;

            await Context.SaveChangesAsync();
        }

        public async Task DeletePersonFromCast(int id, int personId, string role)
        {
            var movie = await GetSingle(id);

            if (movie == null)
            {
                return;
            }

            var castMember = movie.MovieCast.FirstOrDefault(p => p.PersonId == personId && p.Role == role);

            movie.MovieCast.Remove(castMember);

            await Context.SaveChangesAsync();
        }

        public async Task DeleteGenre(int id, string genreName)
        {
            var movie = await GetSingle(id);

            if (movie == null)
            {
                return;
            }

            var genre = movie.MovieGenre.FirstOrDefault(g => g.GenreName == genreName);

            movie.MovieGenre.Remove(genre);

            await Context.SaveChangesAsync();
        }

        public async Task DeleteDirector(int id, int personId)
        {
            var movie = await GetSingle(id);

            if (movie == null)
            {
                return;
            }

            var director = movie.MovieDirector.FirstOrDefault(p => p.PersonId == personId);

            movie.MovieDirector.Remove(director);

            await Context.SaveChangesAsync();
        }

        public async Task AddDirector(int id, int personId)
        {
            var movie = await GetSingle(id);

            if (movie == null)
            {
                return;
            }

            movie.MovieDirector.Add(new MovieDirector
            {
                PersonId = personId
            });

            await Context.SaveChangesAsync();
        }

        public override async Task<Movie> GetSingle(int id)
        {
            return await Context.Movie
                .Include(m => m.MovieDirector).ThenInclude(d => d.Person)
                .Include(m => m.MovieAward).ThenInclude(m => m.Person)
                .Include(m => m.MovieCast).ThenInclude(d => d.Person)
                .Include(m => m.MovieGenre)
                .Include(m => m.PreviousPartNavigation)
                .FirstOrDefaultAsync(m => m.MovieId == id);
        }

        public async Task<Movie> GetWithoutRelations(int id)
        {
            return await Context.Movie.FirstOrDefaultAsync(m => m.MovieId == id);
        }

        public Movie GetRandomMovie()
        {
            var random = new Random();
            int skip = random.Next(0, Context.Movie.Count());
            return Context.Movie.Skip(skip).Take(1).AsNoTracking().First();
        }

        private void AddNewGenres(List<string> genres, Movie movie)
        {
            var newGenres =
                genres.Where(genreName => movie.MovieGenre.All(movieGenre => movieGenre.GenreName != genreName));

            newGenres.ToList().ForEach(genreName =>
            {
                movie.MovieGenre.Add(new MovieGenre
                {
                    GenreName = genreName
                });
            });
        }

        public async Task<List<UserFeedback>> GetComments(int movieId, int page, int limit)
        {
            return await Context.UserFeedback.Include(u => u.User).Where(m => m.MovieId == movieId)
                .OrderByDescending(m => m.Date).Skip(page * limit).Take(limit).AsNoTracking().ToListAsync();
        }

        public async Task<int> CountComments(int movieId)
        {
            return await Context.UserFeedback.Where(m => m.MovieId == movieId).CountAsync();
        }

        public async Task AddComment(int movieId, string userId, string comment, int rating)
        {
            var movie = await GetSingle(movieId);

            if (movie == null)
            {
                return;
            }

            movie.UserFeedback.Add(new UserFeedback
            {
                UserId = userId,
                Comment = comment,
                Rating = rating,
                Date = DateTime.Now
            });

            await Context.SaveChangesAsync();
        }

        public void AddComment(int movieId, string userId, string comment, int rating, DateTime date)
        {
            var movie = Context.Movie.Find(movieId);

            if (movie == null)
            {
                return;
            }

            movie.UserFeedback.Add(new UserFeedback
            {
                UserId = userId,
                Comment = comment,
                Rating = rating,
                Date = date
            });

            Context.SaveChanges();
        }

        public async Task<bool> IsCommentFromUserId(long commentId, string userId)
        {
            var comment = await Context.UserFeedback.FindAsync(commentId);

            if (comment == null)
            {
                return false;
            }

            return comment.UserId == userId;
        }

        public async Task DeleteComment(long commentId)
        {
            var comment = await Context.UserFeedback.FindAsync(commentId);

            if (comment == null)
            {
                return;
            }

            Context.UserFeedback.Remove(comment);

            await Context.SaveChangesAsync();
        }

        public async Task EditComment(long commentId, string comment, int rating)
        {
            var dbComment = await Context.UserFeedback.FindAsync(commentId);

            if (dbComment == null)
            {
                return;
            }

            dbComment.Comment = comment;
            dbComment.Rating = rating;

            await Context.SaveChangesAsync();
        }

        public async Task AddAward(int movieId, string name, string type, int personId, string result, int year)
        {
            var movie = await GetSingle(movieId);

            if (movie == null)
            {
                return;
            }

            movie.MovieAward.Add(new MovieAward
            {
                Name = name,
                Type = type,
                PersonId = personId,
                Result = result,
                Year = year
            });

            await Context.SaveChangesAsync();
        }

        public async Task EditAward(int id, string name, string type, int personId, string result, int year)
        {
            var award = await Context.MovieAward.FindAsync(id);

            if (award == null)
            {
                return;
            }

            award.Name = name;
            award.Type = type;
            award.PersonId = personId;
            award.Result = result;
            award.Year = year;

            await Context.SaveChangesAsync();
        }

        public async Task<List<Person>> GetCast(int movieId)
        {
            return await Context.MovieCast
                .Include(c => c.Person)
                .Where(c => c.MovieId == movieId)
                .Select(c => c.Person)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task DeleteAward(int id, int personId, string awardName, string type, int year)
        {
            var award = Context.MovieAward.FirstOrDefault(
                p => p.MovieId == id && p.PersonId == personId && p.Type == type && p.Name == awardName &&
                     p.Year == year);

            if (award == null)
            {
                return;
            }

            Context.MovieAward.Remove(award);

            await Context.SaveChangesAsync();
        }

        public async Task<MovieAward> GetAward(int id)
        {
            return await Context.MovieAward.FindAsync(id);
        }

        public async Task<List<Movie>> GetMoviesGroupedByYear(int page, int limit)
        {
            return await Context.Movie
                .Include(m => m.MovieAward)
                .ThenInclude(a => a.Person)
                .OrderByDescending(m => m.PublicationYear)
                .Skip(page * limit)
                .Take(limit)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<Movie>> GetMoviesGroupedByYear(int? fromYear, int? tillYear, int page, int limit)
        {
            var query = Context.Movie
                .Include(m => m.MovieAward)
                .ThenInclude(a => a.Person)
                .AsQueryable();

            if (fromYear.HasValue)
            {
                query = query.Where(m => m.PublicationYear >= fromYear);
            }

            if (tillYear.HasValue)
            {
                query = query.Where(m => tillYear >= m.PublicationYear);
            }

            return await query
                .OrderByDescending(m => m.PublicationYear)
                .Skip(page * limit)
                .Take(limit)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<int> CountMoviesGroupedByYear(int? fromYear, int? tillYear)
        {
            var query = Context.Movie.AsQueryable();

            if (fromYear.HasValue)
            {
                query = query.Where(m => m.PublicationYear >= fromYear);
            }

            if (tillYear.HasValue)
            {
                query = query.Where(m => tillYear >= m.PublicationYear);
            }

            return await query
                .CountAsync();
        }

        public async Task<int> CountMoviesGroupedByYear()
        {
            return await Context.Movie.CountAsync();
        }

        public async Task<List<Movie>> GetMostUnPopularMoviesEver(int page, int limit)
        {
            return
                 await Context.Movie.FromSql(
                         "SELECT [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL], AVG(rating) AS average " +
                         "FROM [Movie] AS [m] " +
                         "LEFT JOIN [UserFeedback] AS [c] ON [m].[movie_id] = [c].[MovieId] " +
                         "GROUP By [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL]" +
                         "ORDER BY average ASC " +
                         GetOffsetQuery(page, limit))
                     .ToListAsync();
        }

        public async Task<List<Movie>> GetMoviesOrderedByRatingPrice(int page, int limit)
        {
            return
               await Context.Movie.FromSql(
                       "SELECT [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL], AVG(rating) / [m].[price]  AS ratingPrice " +
                       "FROM [Movie] AS [m] " +
                       "LEFT JOIN [UserFeedback] AS [c] ON [m].[movie_id] = [c].[MovieId] " +
                       "GROUP By [m].[movie_id], [m].[cover_image], [m].[description], [m].[duration], [m].[previous_part], [m].[price], [m].[publication_year], [m].[title], [m].[URL]" +
                       "ORDER BY ratingPrice DESC " +
                       GetOffsetQuery(page, limit))
                   .ToListAsync();
        }

        public async Task<int> CountMoviesWithFeedBack()
        {
            return await Context.Movie.CountAsync(x => x.UserFeedback.Any());
        }

        private void RemoveGenres(List<string> genres, Movie movie)
        {
            var removeEntries = movie.MovieGenre.Where(g => genres.All(pGenres => g.GenreName != pGenres));

            removeEntries.ToList().ForEach(genre => { movie.MovieGenre.Remove(genre); });
        }

        private string GetWhereQuery(int? year, decimal? minPrice,
            decimal? maxPrice, bool includeWhere)
        {
            List<string> statements = new List<string>();
            var stringBuilder = new StringBuilder();

            if (year.HasValue)
            {
                statements.Add("[m].[publication_year] = " + year);
            }

            if (minPrice.HasValue)
            {
                statements.Add("[m].[price] >= " + minPrice);
            }

            if (maxPrice.HasValue)
            {
                statements.Add("[m].[price] <= " + maxPrice);
            }

            if (includeWhere && statements.Any())
            {
                stringBuilder.Append("WHERE ");
            }

            if (!includeWhere && statements.Any())
            {
                stringBuilder.Append("AND ");
            }

            for (int i = 0; statements.Count > i; i++)
            {
                stringBuilder.Append(statements[i]);

                if (statements.Count - i > 1)
                {
                    stringBuilder.Append(" AND ");
                }
            }

            return stringBuilder.ToString();
        }

        private IQueryable<Movie> WhereFilter(int? year, decimal? minPrice, decimal? maxPrice,
            IQueryable<Movie> query)
        {
            if (minPrice.HasValue)
            {
                query = query.Where(m => m.Price >= minPrice);
            }

            if (maxPrice.HasValue)
            {
                query = query.Where(m => m.Price <= maxPrice);
            }

            if (year.HasValue)
            {
                query = query.Where(m => m.PublicationYear == year);
            }

            /*if (genres.Any())
            {
                query = query. .Where(
                        m => m.t.movie.MovieGenre.All(genre => genres.Any(genreName => genreName == genre.GenreName)))));
            }*/

            return query;
        }

        private string GetOffsetQuery(int page, int limit)
        {
            return $"OFFSET ({page * limit}) ROWS FETCH NEXT ({limit}) ROWS ONLY";
        }
    }
}