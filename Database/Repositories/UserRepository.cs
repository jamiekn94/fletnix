﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Interfaces;
using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class UserRepository : Repository<User, string>, IUserRepository
    {
        public UserRepository(FletnixContext fletnixContext)  : base(fletnixContext) { }

        public User GetRandomUser()
        {
            var random = new Random();
            int skip = random.Next(0, Context.Users.Count());
            return Context.Users.Skip(skip).Take(1).First();
        }
    }
}
