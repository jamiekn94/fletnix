﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Interfaces;
using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class CountryRepository : Repository<Country, string>, ICountryRepository
    {
        public CountryRepository(FletnixContext fletnixContext)  : base(fletnixContext) { }

        public async Task<List<string>> GetAll()
        {
            return await Context.Country.Select(dbSet => dbSet.CountryName).ToListAsync();
        }
    }
}
