﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Database.Interfaces;
using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database.Repositories
{
    public class GenreRepository : Repository<Genre, string>, IGenreRepository
    {
        public GenreRepository(FletnixContext fletnixContext) : base(fletnixContext) { }

        public async Task<List<Genre>> GetAll()
        {
            return await Context.Genre.ToListAsync();
        }
    }
}
