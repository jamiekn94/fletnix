﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.Models;

namespace Database
{
    public static class Testing
    {
        static int CountMoviesWithGenre(this List<MovieGenre> movieGenre, List<string> genreNames)
        {
            return movieGenre
                .Where(mg => genreNames.Contains(mg.GenreName))
                .Select(m => m.MovieId)
                .Distinct()
                .Count();
        }
    }
}
