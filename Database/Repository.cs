﻿using System.Threading.Tasks;
using Database.Models;

namespace Database
{
    public abstract class Repository<TEntity, TId> where TEntity : class
    {
        protected readonly FletnixContext Context;

        protected Repository(FletnixContext context)
        {
            Context = context;
        }

        public virtual async Task Add(TEntity item)
        {
            Context.Add(item);

            await Context.SaveChangesAsync();
        }

        public virtual async Task Delete(TId id)
        {
            var item = await GetSingle(id);

            if (item != null)
            {
                Context.Remove(item);

                await Context.SaveChangesAsync();
            }
        }


        public virtual async Task<TEntity> GetSingle(TId id)
        {
            return (TEntity)await Context.FindAsync(typeof(TEntity), id);
        }
    }
}