﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Database.Models;

namespace Database.Items
{
    public class MovieListItem
    {
        public MovieListItem()
        {
            MovieAward = new HashSet<MovieAward>();
            MovieCast = new HashSet<MovieCast>();
            MovieDirector = new HashSet<MovieDirector>();
            MovieGenre = new HashSet<MovieGenre>();
        }

        public int MovieId { get; set; }
        public string Title { get; set; }
        public int? Duration { get; set; }

        public string Description { get; set; }

        [DisplayName("Published")]
        public int? PublicationYear { get; set; }

        [DisplayName("Cover")]
        public byte[] CoverImage { get; set; }

        [DisplayName("Successor")]
        public int? PreviousPart { get; set; }
        public decimal Price { get; set; }
        public string Url { get; set; }

        [DisplayName("Awards")]
        public virtual ICollection<MovieAward> MovieAward { get; set; }

        [DisplayName("Cast")]
        public virtual ICollection<MovieCast> MovieCast { get; set; }

        [DisplayName("Directors")]
        public virtual ICollection<MovieDirector> MovieDirector { get; set; }

        [DisplayName("Genres")]
        public virtual ICollection<MovieGenre> MovieGenre { get; set; }

        public int Watched { get; set; }
    }
}
