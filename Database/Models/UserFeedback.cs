﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Models
{
    public partial class UserFeedback
    {
        public Int64 Id { get; set; }

        public int MovieId { get; set; }

        public string UserId { get; set; }

        public int Rating { get; set; }

        public string Comment { get; set; }

        public DateTime Date { get; set; }

        public virtual Movie Movie { get; set; }

        public virtual User User { get; set; }
    }
}