﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Database.Models
{
    public class UserRole : IdentityRole
    {
        public string Description { get; set; }
    }
}
