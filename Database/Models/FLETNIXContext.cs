﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Database.Models
{
    public partial class FletnixContext : IdentityDbContext<User, UserRole, string>
    {
        public virtual DbSet<Award> Award { get; set; }
        public virtual DbSet<AwardType> AwardType { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<Genre> Genre { get; set; }
        public virtual DbSet<Movie> Movie { get; set; }
        public virtual DbSet<MovieAward> MovieAward { get; set; }
        public virtual DbSet<MovieCast> MovieCast { get; set; }
        public virtual DbSet<MovieDirector> MovieDirector { get; set; }
        public virtual DbSet<MovieGenre> MovieGenre { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<UserFeedback> UserFeedback { get; set; }
        public virtual DbSet<WatchHistory> WatchHistory { get; set; }

        // Unable to generate entity type for table 'dbo.Watchhistory'. Please see the warning messages.

        public FletnixContext(DbContextOptions<FletnixContext> options) : base(options)

        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Award>(entity =>
            {
                entity.HasKey(e => e.Name)
                    .HasName("pk_Award");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<AwardType>(entity =>
            {
                entity.HasKey(e => new { e.Name, e.Type })
                    .HasName("pk_AwardType");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasKey(e => e.CountryName)
                    .HasName("PK_Country");

                entity.Property(e => e.CountryName)
                    .HasColumnName("country_name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.PaypalAccount)
                    .HasName("AK_Customer_Paypal")
                    .IsUnique();

                entity.Property(e => e.Id).HasMaxLength(450);

                entity.Property(e => e.CountryName)
                    .IsRequired()
                    .HasColumnName("country_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PaypalAccount)
                    .IsRequired()
                    .HasColumnName("paypal_account")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.SubscriptionEnd)
                    .HasColumnName("subscription_end")
                    .HasColumnType("date");

                entity.Property(e => e.SubscriptionStart)
                    .HasColumnName("subscription_start")
                    .HasColumnType("date");

                entity.HasOne(d => d.CountryNameNavigation)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.CountryName)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_country");
            });

            modelBuilder.Entity<Event>(entity =>
            {
                entity.HasKey(e => new { e.Name, e.Year })
                    .HasName("pk_Event");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Year).HasColumnName("year");

                entity.Property(e => e.Location)
                    .IsRequired()
                    .HasColumnName("location")
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.LocationNavigation)
                    .WithMany(p => p.Event)
                    .HasForeignKey(d => d.Location)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("fk_Event_Country");

                entity.HasOne(d => d.NameNavigation)
                    .WithMany(p => p.Event)
                    .HasForeignKey(d => d.Name)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("fk_Event_Award");
            });

            modelBuilder.Entity<Genre>(entity =>
            {
                entity.HasKey(e => e.GenreName)
                    .HasName("PK_Genre_1");

                entity.Property(e => e.GenreName)
                    .HasColumnName("genre_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<Movie>(entity =>
            {
                entity.HasIndex(e => e.Title)
                    .HasName("blabla");

                entity.Property(e => e.MovieId)
                    .HasColumnName("movie_id");

                entity.Property(e => e.CoverImage)
                    .HasColumnName("cover_image")
                    .HasColumnType("varbinary(MAX)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Duration).HasColumnName("duration");

                entity.Property(e => e.PreviousPart).HasColumnName("previous_part");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric");

                entity.Property(e => e.PublicationYear).HasColumnName("publication_year");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Url)
                    .HasColumnName("URL")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.PreviousPartNavigation)
                    .WithMany(p => p.InversePreviousPartNavigation)
                    .HasForeignKey(d => d.PreviousPart)
                    .HasConstraintName("FK_previous_part");
            });

            modelBuilder.Entity<MovieAward>(entity =>
            {
                entity.HasKey(e => new { e.Id })
                    .HasName("pk_MovieAward");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Year).HasColumnName("year");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.PersonId).HasColumnName("person_id");

                entity.Property(e => e.Result)
                    .IsRequired()
                    .HasColumnName("result")
                    .HasColumnType("varchar(10)");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieAward)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("fk_Event_Movie");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.MovieAward)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("fk_Event_Person");

                entity.HasOne(d => d.AwardType)
                    .WithMany(p => p.MovieAward)
                    .HasForeignKey(d => new { d.Name, d.Type })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("fk_Event_AwardType");
            });

            modelBuilder.Entity<MovieCast>(entity =>
            {
                entity.HasKey(e => new { e.Id })
                    .HasName("PK_Movie_Cast_1");

                entity.HasIndex(e => new { e.MovieId, e.PersonId, e.Role })
                    .HasName("IX_Movie_Cast");

                entity.ToTable("Movie_Cast");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.PersonId).HasColumnName("person_id");

                entity.Property(e => e.Role)
                    .HasColumnName("role")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieCast)
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK2_movie_id");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.MovieCast)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK2_person_id");
            });

            modelBuilder.Entity<MovieDirector>(entity =>
            {
                entity.HasKey(e => new { e.MovieId, e.PersonId })
                    .HasName("PK_Movie_Directors");

                entity.ToTable("Movie_Director");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.PersonId).HasColumnName("person_id");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieDirector)
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_movie_id");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.MovieDirector)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_person_id");
            });

            modelBuilder.Entity<MovieGenre>(entity =>
            {
                entity.HasKey(e => new { e.MovieId, e.GenreName })
                    .HasName("PK_Movie_Genre");

                entity.ToTable("Movie_Genre");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.GenreName)
                    .HasColumnName("genre_name")
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.GenreNameNavigation)
                    .WithMany(p => p.MovieGenre)
                    .HasForeignKey(d => d.GenreName)
                    .HasConstraintName("FK_genre");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieGenre)
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK3_movie_id");
            });

            modelBuilder.Entity<UserFeedback>(entity =>
            {
                entity.HasKey(e => new { e.Id})
                    .HasName("PK_UserFeedback");

                entity.ToTable("UserFeedback");

                entity.Property(e => e.MovieId)
                    .HasColumnName("MovieId")
                    .HasColumnType("int");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserId")
                    .HasColumnType("nvarchar(450)");

                entity.Property(e => e.Rating)
                    .HasColumnName("Rating")
                    .HasColumnType("int");

                entity.Property(e => e.Date)
                    .HasColumnName("Date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.UserFeedback)
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_UserFeedback_Movie");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.PersonId)
                    .HasColumnName("person_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasColumnType("char(1)");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WatchHistory>(entity =>
            {
                entity.HasKey(e => new { e.Id })
                    .HasName("PK_WatchHistory");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("nvarchar(450)");

                entity.Property(e => e.MovieId)
                    .HasColumnName("movie_id")
                    .HasColumnType("int");

                entity.Property(e => e.WatchDate)
                    .HasColumnName("watch_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric(5, 2)");

                entity.Property(e => e.Invoiced)
                    .HasColumnName("invoiced")
                    .HasColumnType("bit");
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}