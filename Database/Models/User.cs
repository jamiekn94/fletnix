﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Database.Models
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime SubscriptionStart { get; set; }
        
        public DateTime SubscriptionEnd { get; set; }
        
        public string CountryName { get; set; }
        
        public string PaypalAccount { get; set; }
    }
}
