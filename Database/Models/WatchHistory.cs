﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Database.Models
{
    public partial class WatchHistory
    {
        public Int64 Id { get; set; }
        public string UserId { get; set; }
        public int MovieId { get; set; }
        [DisplayName("Date watched")]
        public DateTime WatchDate { get; set; }
        public decimal Price { get; set; }
        public bool Invoiced { get; set; }
        public virtual Movie Movie { get; set; }
        public virtual User User { get; set; }
    }
}
