﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Database.Models;

namespace Database.Interfaces
{
    public interface IWatchHistoryRepository
    {
        Task Add(string userId, Movie movie, DateTime date);
        Task<List<WatchHistory>> GetByUserId(string userId, int page, int limit);
        Task<int> CountByUserId(string userId);
        Task<bool> HasWatched(string userId, int movieId);
    }
}
