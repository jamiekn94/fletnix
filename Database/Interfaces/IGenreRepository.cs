﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Database.Models;

namespace Database.Interfaces
{
    public interface IGenreRepository
    {
        Task<List<Genre>> GetAll();
    }
}
