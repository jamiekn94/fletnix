﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Database.Models;

namespace Database.Interfaces
{
    public interface IPersonRepository
    {
        Task<List<Person>> GetAll();

        Task<List<Person>> GetAll(int page, int limit);

        Task<List<Person>> Find(string name);

        Task Add(string firstName, string lastName, string gender);

        Task<int> Count();

        Task Edit(int id, string firstName, string lastName, string gender);

        Task<Person> GetSingle(int id);
        Task Remove(int id);
    }
}
