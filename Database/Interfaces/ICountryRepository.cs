﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Database.Interfaces
{
    public interface ICountryRepository
    {
        Task<List<string>> GetAll();
    }
}
