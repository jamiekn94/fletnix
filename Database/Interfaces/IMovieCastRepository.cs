﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Database.Models;

namespace Database.Interfaces
{
    public interface IMovieCastRepository
    {
        Task<List<MovieCast>> GetAll();

        Task Add(MovieCast cast);
    }
}
