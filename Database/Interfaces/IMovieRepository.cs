﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Items;
using Database.Models;

namespace Database.Interfaces
{
    public interface IMovieRepository
    {
        Task<List<Movie>> GetPage(int skip, int limit);

        Task<List<string>> GetNames();

        Task<Movie> GetSingle(int id);

        Task Delete(int id);

        Task Add(Movie item);

        Task Update(int id, string title, int? duration, string description, int? year, byte[] coverImage, decimal price, string url, List<string> genres);

        Task UpdateCast(int movieId, int personId, string role);

        Task DeletePersonFromCast(int id, int personId, string role);
        Task<int> CountMoviesGroupedByYear();

        Task DeleteGenre(int id, string genreName);

        Task DeleteDirector(int id, int personId);

        Task AddDirector(int id, int personId);

        Task<List<Movie>> GetMoviesGroupedByYear(int? fromYear, int? tillYear, int page, int limit);

        Task<List<Movie>> GetMostPopularMoviesEver(int page, int limit);

        Task<List<Movie>> GetMostPopularMoviesEver(List<string> genres, int? year, decimal? minPrice, decimal? maxPrice, int page,
            int limit);

        Task<List<Movie>> GetMostPopularMoviesOfLastTwoWeeks(List<string> genres, int? year, decimal? minPrice, decimal? maxPrice, int page,
            int limit);

        Task<List<Movie>> GetAll(List<string> genres, int? year, decimal? minPrice, decimal? maxPrice, int page,
            int limit);

        Task<int> CountMovies(List<string> genres, int? year, decimal? minPrice,
            decimal? maxPrice);

        Task<int> CountMovies();

        Task<List<UserFeedback>> GetComments(int movieId, int page, int limit);

        Task<int> CountComments(int movieId);

        Task AddComment(int movieId, string userId, string comment, int rating);

        void AddComment(int movieId, string userId, string comment, int rating, DateTime date);

        Task<bool> IsCommentFromUserId(long commentId, string userId);

        Task DeleteComment(long commentId);

        Task EditComment(long commentId, string comment, int rating);

        Task AddAward(int movieId, string name, string type, int personId, string result, int year);

        Task EditAward(int id, string name, string type, int personId, string result, int year);

        Task<List<Person>> GetCast(int movieId);

        Task DeleteAward(int id, int personId, string awardName, string type, int year);

        Task<MovieAward> GetAward(int id);

        Task<List<Movie>> GetMoviesGroupedByYear(int page, int limit);

        Task<int> CountMoviesGroupedByYear(int? fromYear, int? tillYear);

        Task<int> CountMostPopularMoviesOfLastTwoWeeksList(List<string> genres, int? year, decimal? minPrice, decimal? maxPrice);

        Movie GetRandomMovie();

        Task<Movie> GetWithoutRelations(int id);

        Task<List<Movie>> GetMostUnPopularMoviesEver(int page, int limit);

        Task<List<Movie>> GetMoviesOrderedByRatingPrice(int page, int limit);

        Task<int> CountMoviesWithFeedBack();
    }
}
