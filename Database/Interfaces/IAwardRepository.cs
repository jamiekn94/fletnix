﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Database.Interfaces
{
    public interface IAwardRepository
    {
        Task<List<string>> GetAll();
        Task<List<string>> GetAwardTypes(string name);
    }
}
