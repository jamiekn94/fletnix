﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Database.Interfaces;
using Database.Models;
using Database.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Database
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IServiceProvider _serviceProvider;

        public UnitOfWork(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public ICountryRepository CountryRepository => new CountryRepository(CreateContext());

        public IMovieRepository MovieRepository => new MovieRepository(CreateContext());

        public IPersonRepository PersonRepository => new PersonRepository(CreateContext());

        public IGenreRepository GenreRepository => new GenreRepository(CreateContext());

        public IMovieCastRepository MovieCastRepository => new MovieCastRepository(CreateContext());

        public IWatchHistoryRepository WatchHistoryRepository => new WatchHistoryRepository(CreateContext());

        public IAwardRepository AwardRepository => new AwardRepository(CreateContext());

        public IUserRepository UserRepository => new UserRepository(CreateContext());

        private FletnixContext CreateContext()
        {
            return _serviceProvider.GetService<FletnixContext>();
        }
    }
}