﻿using System.Threading.Tasks;
using Database.Interfaces;

namespace Database
{
    public interface IUnitOfWork
    {
        ICountryRepository CountryRepository { get; }

        IMovieRepository MovieRepository { get; }

        IPersonRepository PersonRepository { get; }

        IGenreRepository GenreRepository { get; }

        IMovieCastRepository MovieCastRepository { get; }

        IWatchHistoryRepository WatchHistoryRepository { get; }

        IAwardRepository AwardRepository { get; }

        IUserRepository UserRepository { get; }
    }
}