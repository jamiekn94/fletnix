﻿using System.IdentityModel.Tokens.Jwt;
using Database;
using Database.Models;
using FletNix.Services;
using FletNix.Services.Interfaces;
using Global;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FletNix
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();

            services.AddMvc(options =>
            {
                options.Filters.Add(new XFrameHeader());
                options.Filters.Add(new XSSHeader());
            });

            services.AddDbContext<FletnixContext>(options =>
                options.UseSqlServer(Database.Config.ConnectionString), ServiceLifetime.Transient);

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IMovieCachingProvider, MovieInMemoryCachingProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            /* app.UseMiddleware<StackifyMiddleware.RequestTracerMiddleware>();*/
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            app.Use((context, next) =>
            {
                context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                return next();
            });

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = "Cookies",
                CookieHttpOnly = true,
                CookieSecure = CookieSecurePolicy.SameAsRequest
            });

            app.UseOpenIdConnectAuthentication(new OpenIdConnectOptions
            {
                AuthenticationScheme = "oidc",
                SignInScheme = "Cookies",
                
                Authority = Config.Settings.IsDevelopment ? "http://localhost:5000" : "http://fletnixidentity.azurewebsites.net",
                RequireHttpsMetadata = false,

                ClientId = "mvc",
                ClientSecret = "secret",

                ResponseType = "code id_token",
                Scope = { "Role" },

                GetClaimsFromUserInfoEndpoint = true,
                SaveTokens = true
            });

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
