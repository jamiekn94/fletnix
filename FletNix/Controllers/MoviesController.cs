using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Database;
using Database.Items;
using Database.Models;
using FletNix.Services.Interfaces;
using FletNix.ViewModels.Movie_overview;
using FletNix.ViewModels.Movie_watch;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FletNix.Controllers
{
    public enum Category
    {
        MostPopulairEver = 0,
        MostPopulairLastTwoWeeks = 1,
        Alphabetical = 2
    }

    [Authorize]
    public class MoviesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMovieCachingProvider _movieCachingProvider;
        private const int LIMIT = 12;

        public MoviesController(IUnitOfWork unitOfWork, IMovieCachingProvider movieCachingProvider)
        {
            _unitOfWork = unitOfWork;
            _movieCachingProvider = movieCachingProvider;
        }

        public async Task<IActionResult> Index()
        {
            var movies = _unitOfWork.MovieRepository.GetMostPopularMoviesEver(0, LIMIT);
            var amountMovies = _unitOfWork.MovieRepository.CountMovies();

            ViewData["categories"] = GetCategories();
            ViewData["movies"] = await movies;
            ViewData["amountMovies"] = await amountMovies;
            ViewData["hasNextPage"] = await amountMovies > 0 * LIMIT;
            ViewData["hasPreviousPage"] = false;
            ViewData["page"] = 0;

            return View(new SearchMovie(0));
        }

        public async Task<IActionResult> Search(SearchMovie model, int page = 0)
        {
            Task<List<Movie>> movies = null;

            if (ModelState.IsValid)
            {
                movies = GetMovies(model, page);
            }
            else
            {
                ViewData["movies"] = new List<Movie>();
            }
            
            var amountMovies = GetMoviesCount(model);

            ViewData["categories"] = GetCategories();
            ViewData["amountMovies"] = await amountMovies;
            ViewData["hasNextPage"] = await amountMovies > (page + 1) * LIMIT;
            ViewData["hasPreviousPage"] = page > 0;
            ViewData["page"] = page;

            if (movies != null) ViewData["movies"] = await movies;

            return View("Index", model);
        }

        public async Task<IActionResult> Watch(int id, int commentsPage = 0)
        {
            var movie = await LoadMovie(id, commentsPage, HttpContext.User.FindFirst("sub").Value);
            await AddWatchHistory(movie);

            if (movie == null)
            {
                return NotFound();
            }

            return View(new WatchMovie(id));
        }

        public async Task<IActionResult> NonCachedWatch(int id, int commentsPage = 0)
        {
            var movie = await LoadNonCachedMovie(id, commentsPage);
            await AddWatchHistory(movie);

            if (movie == null)
            {
                return NotFound();
            }

            return View(new WatchMovie(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Comment(WatchMovie model)
        {
            if (ModelState.IsValid)
            {
                await _unitOfWork.MovieRepository.AddComment(model.WriteComment.MovieId,
                    HttpContext.User.FindFirst("sub").Value,
                    model.WriteComment.Comment, model.WriteComment.Rating);

                return RedirectToAction("Watch", new {id = model.WriteComment.MovieId});
            }

            await LoadMovie(model.WriteComment.MovieId, 0, HttpContext.User.FindFirst("sub").Value);

            return View("Watch", model);
        }

        public IActionResult Comment()
        {
            return RedirectToAction("Index");
        }

        public IActionResult EditComment()
        {
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteComment(int id, int commentId)
        {
            if (await _unitOfWork.MovieRepository.IsCommentFromUserId(commentId,
                HttpContext.User.FindFirst("sub").Value))
            {
                await _unitOfWork.MovieRepository.DeleteComment(commentId);
            }

            return RedirectToAction("Watch", new {id});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditComment(UpdateCommentModel updateComment)
        {
            if (ModelState.IsValid)
            {
                if (await _unitOfWork.MovieRepository.IsCommentFromUserId(updateComment.CommentId,
                    HttpContext.User.FindFirst("sub").Value))
                {
                    await _unitOfWork.MovieRepository.EditComment(updateComment.CommentId,
                        updateComment.Comment, updateComment.Rating);

                    return RedirectToAction("Watch", new {id = updateComment.MovieId});
                }
            }

            await LoadMovie(updateComment.MovieId, 0, HttpContext.User.FindFirst("sub").Value);
            return View("Watch", new WatchMovie(updateComment));
        }

        private async Task<List<Movie>> GetMovies(SearchMovie search, int page)
        {
             switch ((Category) search.Category)
             {
                 case Category.MostPopulairEver:
                 {
                     return await _unitOfWork.MovieRepository.GetMostPopularMoviesEver(search.Genres, search.Year,
                         search.MinPrice,
                         search.MaxPrice, page, LIMIT);
                 }
                 case Category.MostPopulairLastTwoWeeks:
                 {
                     return await _unitOfWork.MovieRepository.GetMostPopularMoviesOfLastTwoWeeks(search.Genres,
                         search.Year, search.MinPrice,
                         search.MaxPrice, page, LIMIT);
                 }
                 case Category.Alphabetical:
                 {
                     return await _unitOfWork.MovieRepository.GetAll(search.Genres, search.Year, search.MinPrice,
                         search.MaxPrice, page, LIMIT);
                 }
             }

             throw new ArgumentException($"No category found for: {search.Category}");
        }

        private async Task<int> GetMoviesCount(SearchMovie search)
        {
            switch ((Category) search.Category)
            {
                case Category.MostPopulairLastTwoWeeks:
                {
                    return await _unitOfWork.MovieRepository.CountMostPopularMoviesOfLastTwoWeeksList(search.Genres,
                        search.Year, search.MinPrice,
                        search.MaxPrice);
                }
                default:
                {
                    return await _unitOfWork.MovieRepository.CountMovies(search.Genres,
                        search.Year, search.MinPrice,
                        search.MaxPrice);
                }
            }
        }

        private async Task<List<Movie>> GetSeries(Movie movie)
        {
            List<Movie> movies = new List<Movie>();

            Movie successor = movie;

            while (true)
            {
                successor = await GetSuccessor(successor);

                if (successor == null)
                {
                    break;
                }

                movies.Add(successor);
            }

            return movies;
        }

        private async Task<Movie> GetSuccessor(Movie movie)
        {
            if (movie.PreviousPart.HasValue)
            {
                return await _movieCachingProvider.GetById(movie.PreviousPart.Value);
            }

            return null;
        }

        private async Task<Movie> LoadMovie(int id, int commentsPage, string userId)
        {
            const int commentsLimit = 10;

            var movie = await _movieCachingProvider.GetById(id);

            var comments = _unitOfWork.MovieRepository.GetComments(id, commentsPage, commentsLimit);
            var series = GetSeries(movie);
            var amountComments = _unitOfWork.MovieRepository.CountComments(id);

            ViewData["series"] = await series;
            ViewData["comments"] = await comments;
            ViewData["page"] = commentsPage;
            ViewData["hasNextPage"] = await amountComments > (commentsPage + 1) * LIMIT;
            ViewData["hasPreviousPage"] = commentsPage > 0;
            ViewData["movie"] = movie;
            ViewData["userId"] = userId;

            return movie;
        }

        private async Task<Movie> LoadNonCachedMovie(int id, int commentsPage)
        {
            const int commentsLimit = 10;

            string userId = HttpContext.User.FindFirst("sub").Value;

            var movie = await _unitOfWork.MovieRepository.GetSingle(id);

            var comments = _unitOfWork.MovieRepository.GetComments(id, commentsPage, commentsLimit);
            var series = GetSeries(movie);
            var amountComments = _unitOfWork.MovieRepository.CountComments(id);

            ViewData["series"] = await series;
            ViewData["comments"] = await comments;
            ViewData["page"] = commentsPage;
            ViewData["hasNextPage"] = await amountComments > (commentsPage + 1) * LIMIT;
            ViewData["hasPreviousPage"] = commentsPage > 0;
            ViewData["movie"] = movie;
            ViewData["userId"] = userId;

            return movie;
        }

        private List<SelectListItem> GetCategories()
        {
            var categoryMostPopulairEver = new SelectListItem
            {
                Value = "0",
                Text = "Meest populaire films ooit"
            };

            var categoryMostPopulairLastTwoWeeks = new SelectListItem
            {
                Value = "1",
                Text = "Meest populaire afgelopen twee weken"
            };

            var categoryAll = new SelectListItem
            {
                Value = "2",
                Text = "Alfabetisch"
            };

            return new List<SelectListItem> {categoryAll, categoryMostPopulairEver, categoryMostPopulairLastTwoWeeks};
        }

        private async Task AddWatchHistory(Movie movie)
        {
            if (!await _unitOfWork.WatchHistoryRepository.HasWatched(HttpContext.User.FindFirst("sub").Value,
                movie.MovieId))
            {
                await _unitOfWork.WatchHistoryRepository.Add(HttpContext.User.FindFirst("sub").Value, movie,
                    DateTime.Now);
            }
        }
    }
}