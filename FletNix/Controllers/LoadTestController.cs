using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database;
using Database.Models;
using FletNix.Services.Interfaces;
using FletNix.ViewModels.Movie_overview;
using FletNix.ViewModels.Movie_watch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FletNix.Controllers
{
    public class LoadTestController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMovieCachingProvider _movieCachingProvider;
        private const int Limit = 12;

        public LoadTestController(IUnitOfWork unitOfWork, IMovieCachingProvider movieCachingProvider)
        {
            _unitOfWork = unitOfWork;
            _movieCachingProvider = movieCachingProvider;
        }

        public async Task<IActionResult> TestCachedWatch(int id, int commentsPage = 0)
        {
            var movie = await LoadMovie(id, commentsPage, "TEST-CASE");

            if (movie == null)
            {
                return NotFound();
            }

            return View("~/Views/Movies/Watch.cshtml", new WatchMovie(id));
        }

        public async Task<IActionResult> TestNonCachedWatch(int id, int commentsPage = 0)
        {
            var movie = await LoadNonCachedMovie(id, commentsPage, "TEST-CASE");

            if (movie == null)
            {
                return NotFound();
            }

            return View("~/Views/Movies/Watch.cshtml", new WatchMovie(id));
        }

        public async Task<IActionResult> TestAsyncSearch(SearchMovie model, int page = 0)
        {
            Task<List<Movie>> movies = null;

            if (ModelState.IsValid)
            {
                movies = GetMovies(model, page);
            }
            else
            {
                ViewData["movies"] = new List<Movie>();
            }

            var amountMovies = GetMoviesCount(model);

            ViewData["categories"] = GetCategories();
            ViewData["amountMovies"] = await amountMovies;
            ViewData["hasNextPage"] = await amountMovies > (page + 1) * Limit;
            ViewData["hasPreviousPage"] = page > 0;
            ViewData["page"] = page;

            if (movies != null) ViewData["movies"] = await movies;

            return View("~/Views/Movies/Index.cshtml", model);
        }

        public IActionResult TestSyncSearch(SearchMovie model, int page = 0)
        {
            List<Movie> movies = new List<Movie>();

            if (ModelState.IsValid)
            {
                movies = GetSyncMovies(model, page);
            }
            else
            {
                ViewData["movies"] = new List<Movie>();
            }

            var amountMovies = GetSyncMoviesCount(model);

            ViewData["categories"] = GetCategories();
            ViewData["amountMovies"] =  amountMovies;
            ViewData["hasNextPage"] = amountMovies > (page + 1) * Limit;
            ViewData["hasPreviousPage"] = page > 0;
            ViewData["page"] = page;

            ViewData["movies"] =  movies;

            return View("~/Views/Movies/Index.cshtml", model);
        }

        private async Task<List<Movie>> GetMovies(SearchMovie search, int page)
        {
            switch ((Category)search.Category)
            {
                case Category.MostPopulairEver:
                    {
                        return await _unitOfWork.MovieRepository.GetMostPopularMoviesEver(search.Genres, search.Year,
                            search.MinPrice,
                            search.MaxPrice, page, Limit);
                    }
                case Category.MostPopulairLastTwoWeeks:
                    {
                        return await _unitOfWork.MovieRepository.GetMostPopularMoviesOfLastTwoWeeks(search.Genres,
                            search.Year, search.MinPrice,
                            search.MaxPrice, page, Limit);
                    }
                case Category.Alphabetical:
                    {
                        return await _unitOfWork.MovieRepository.GetAll(search.Genres, search.Year, search.MinPrice,
                            search.MaxPrice, page, Limit);
                    }
            }

            throw new ArgumentException($"No category found for: {search.Category}");
        }

        private async Task<int> GetMoviesCount(SearchMovie search)
        {
            switch ((Category)search.Category)
            {
                case Category.MostPopulairLastTwoWeeks:
                    {
                        return await _unitOfWork.MovieRepository.CountMostPopularMoviesOfLastTwoWeeksList(search.Genres,
                            search.Year, search.MinPrice,
                            search.MaxPrice);
                    }
                default:
                    {
                        return await _unitOfWork.MovieRepository.CountMovies(search.Genres,
                            search.Year, search.MinPrice,
                            search.MaxPrice);
                    }
            }
        }

        private List<SelectListItem> GetCategories()
        {
            var categoryMostPopulairEver = new SelectListItem
            {
                Value = "0",
                Text = "Meest populaire films ooit"
            };

            var categoryMostPopulairLastTwoWeeks = new SelectListItem
            {
                Value = "1",
                Text = "Meest populaire afgelopen twee weken"
            };

            var categoryAll = new SelectListItem
            {
                Value = "2",
                Text = "Alfabetisch"
            };

            return new List<SelectListItem> { categoryAll, categoryMostPopulairEver, categoryMostPopulairLastTwoWeeks };
        }

        private int GetSyncMoviesCount(SearchMovie search)
        {
            switch ((Category)search.Category)
            {
                case Category.MostPopulairLastTwoWeeks:
                    {
                        return _unitOfWork.MovieRepository.CountMostPopularMoviesOfLastTwoWeeksList(search.Genres,
                            search.Year, search.MinPrice,
                            search.MaxPrice).GetAwaiter().GetResult();
                    }
                default:
                {
                    return _unitOfWork.MovieRepository.CountMovies(search.Genres,
                        search.Year, search.MinPrice,
                        search.MaxPrice).GetAwaiter().GetResult();
                }
            }
        }

        private List<Movie> GetSyncMovies(SearchMovie search, int page)
        {
            switch ((Category)search.Category)
            {
                case Category.MostPopulairEver:
                    {
                        return _unitOfWork.MovieRepository.GetMostPopularMoviesEver(search.Genres, search.Year,
                            search.MinPrice,
                            search.MaxPrice, page, Limit).GetAwaiter().GetResult();
                    }
                case Category.MostPopulairLastTwoWeeks:
                    {
                        return _unitOfWork.MovieRepository.GetMostPopularMoviesOfLastTwoWeeks(search.Genres,
                            search.Year, search.MinPrice,
                            search.MaxPrice, page, Limit).GetAwaiter().GetResult();
                    }
                case Category.Alphabetical:
                    {
                        return _unitOfWork.MovieRepository.GetAll(search.Genres, search.Year, search.MinPrice,
                            search.MaxPrice, page, Limit).GetAwaiter().GetResult();
                    }
            }

            throw new ArgumentException($"No category found for: {search.Category}");
        }

        private async Task<List<Movie>> GetNonCachedSeries(Movie movie)
        {
            List<Movie> movies = new List<Movie>();

            Movie successor = movie;

            while (true)
            {
                successor = await GetNonCachedSuccessor(successor);

                if (successor == null)
                {
                    break;
                }

                movies.Add(successor);
            }

            return movies;
        }

        private async Task<Movie> GetNonCachedSuccessor(Movie movie)
        {
            if (movie.PreviousPart.HasValue)
            {
                return await _unitOfWork.MovieRepository.GetWithoutRelations(movie.PreviousPart.Value);
            }

            return null;
        }

        private async Task<Movie> LoadNonCachedMovie(int id, int commentsPage, string userId)
        {
            const int commentsLimit = 10;

            var movie = await _unitOfWork.MovieRepository.GetSingle(id);

            var comments = _unitOfWork.MovieRepository.GetComments(id, commentsPage, commentsLimit);
            var series = GetNonCachedSeries(movie);
            var amountComments = _unitOfWork.MovieRepository.CountComments(id);

            ViewData["series"] = await series;
            ViewData["comments"] = await comments;
            ViewData["page"] = commentsPage;
            ViewData["hasNextPage"] = await amountComments > (commentsPage + 1) * Limit;
            ViewData["hasPreviousPage"] = commentsPage > 0;
            ViewData["movie"] = movie;
            ViewData["userId"] = userId;

            return movie;
        }

        private async Task<Movie> LoadMovie(int id, int commentsPage, string userId)
        {
            const int commentsLimit = 10;

            var movie = await _movieCachingProvider.GetById(id);

            var comments = _unitOfWork.MovieRepository.GetComments(id, commentsPage, commentsLimit);
            var series = GetSeries(movie);
            var amountComments = _unitOfWork.MovieRepository.CountComments(id);

            ViewData["series"] = await series;
            ViewData["comments"] = await comments;
            ViewData["page"] = commentsPage;
            ViewData["hasNextPage"] = await amountComments > (commentsPage + 1) * Limit;
            ViewData["hasPreviousPage"] = commentsPage > 0;
            ViewData["movie"] = movie;
            ViewData["userId"] = userId;

            return movie;
        }

        private async Task<List<Movie>> GetSeries(Movie movie)
        {
            List<Movie> movies = new List<Movie>();

            Movie successor = movie;

            while (true)
            {
                successor = await GetSuccessor(successor);

                if (successor == null)
                {
                    break;
                }

                movies.Add(successor);
            }

            return movies;
        }

        private async Task<Movie> GetSuccessor(Movie movie)
        {
            if (movie.PreviousPart.HasValue)
            {
                return await _movieCachingProvider.GetById(movie.PreviousPart.Value);
            }

            return null;
        }
    }
}