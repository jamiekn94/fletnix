﻿using System.Threading.Tasks;
using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FletNix.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private const int LIMIT = 20;
        private readonly IUnitOfWork _unitOfWork;

        public AccountController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> History(int page = 0)
        {
            string userId = HttpContext.User.FindFirst("sub").Value;

            var watchHistory = _unitOfWork.WatchHistoryRepository.GetByUserId(userId, page,
                LIMIT);

            var amountWatchedMovies = _unitOfWork.WatchHistoryRepository.CountByUserId(userId);

            ViewData["page"] = page;
            ViewData["amountWatchedMovies"] = await amountWatchedMovies;
            ViewData["hasNextPage"] = await amountWatchedMovies > (page + 1) * LIMIT;
            ViewData["hasPreviousPage"] = page > 0;

            return View(await watchHistory);
        }
    }
}