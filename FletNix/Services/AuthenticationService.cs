﻿using System.Threading.Tasks;
using IdentityModel.Client;

namespace FletNix.Services
{
    public class AuthenticationService
    {
        public async Task SetSession()
        {
            var tokenClient = new TokenClient("http://localhost:5000/connect/token", "mvc", "secret");
            var tokenResponse = await tokenClient.RequestClientCredentialsAsync("api1");

            var userInfoClient = new UserInfoClient("http://localhost:5000/connect/userinfo");

            var response = await userInfoClient.GetAsync(tokenResponse.AccessToken);

            //HttpContext.Authentication.HttpContext.User.

            /* var client = new HttpClient();
             client.SetBearerToken(tokenResponse.AccessToken);
             var content = await client.GetStringAsync("http://localhost:5001/api/identity");

             ViewBag.Json = JArray.Parse(content).ToString();*/
        }
    }
}
