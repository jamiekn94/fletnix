﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace FletNix.Services.Interfaces
{
    public interface ICachingProvider<TEntity, TId> where TEntity : class
    {
        TEntity Get(TId id);

        void Add(TId id, TEntity entity, MemoryCacheEntryOptions options);
    }
}
