﻿using System.Threading.Tasks;
using Database.Models;

namespace FletNix.Services.Interfaces
{
    public interface IMovieCachingProvider
    {
        Task<Movie> GetById(int id);
    }
}
