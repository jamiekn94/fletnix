﻿using System;
using FletNix.Services.Interfaces;
using Microsoft.Extensions.Caching.Memory;

namespace FletNix.Services
{
    public abstract class InMemoryCachingProvider<TEntity, TId> : ICachingProvider<TEntity, TId> where TEntity : class
    {
        private readonly IMemoryCache _memoryCache;
        private readonly string _name;

        protected InMemoryCachingProvider(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
            _name = typeof(TEntity).FullName;
        }

        public TEntity Get(TId id)
        {
            object value;

            _memoryCache.TryGetValue(GetKey(id), out value);

            return (TEntity) value;
        }

        public void Add(TId id, TEntity entity, MemoryCacheEntryOptions options)
        {
            _memoryCache.Set(GetKey(id), entity, options);
        }

        public string GetKey(TId id)
        {
            return $"{_name}-{id}";
        }
    }
}
