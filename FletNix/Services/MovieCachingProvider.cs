﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Database;
using Database.Models;
using FletNix.Services.Interfaces;
using Microsoft.Extensions.Caching.Memory;

namespace FletNix.Services
{
    public class MovieInMemoryCachingProvider : InMemoryCachingProvider<Movie, int>, IMovieCachingProvider
    {
        private readonly IUnitOfWork _unitOfWork;

        public MovieInMemoryCachingProvider(IMemoryCache memoryCache, IUnitOfWork unitOfWork) : base(memoryCache)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Movie> GetById(int id)
        {
            var cachedMovie = Get(id);

            if (cachedMovie != null)
            {
                Debug.WriteLine($"Movie cache hit: '{id}'.");
                return cachedMovie;
            }

            var dbMovie = await _unitOfWork.MovieRepository.GetSingle(id);

            Add(id, dbMovie, GetMemoryCacheEntryOptions());

            return dbMovie;
        }

        private MemoryCacheEntryOptions GetMemoryCacheEntryOptions()
        {
            return new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(15)
            };
        }
    }
}
