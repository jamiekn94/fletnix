﻿using System.ComponentModel.DataAnnotations;

namespace FletNix.ViewModels.Movie_watch
{
    public class UpdateCommentModel : WriteCommentModel
    {
        [Required]
        public long CommentId { get; set; }
    }
}
