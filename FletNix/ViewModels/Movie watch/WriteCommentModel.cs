﻿using System.ComponentModel.DataAnnotations;

namespace FletNix.ViewModels.Movie_watch
{
    public class WriteCommentModel
    {
        public WriteCommentModel() { }

        public WriteCommentModel(int id)
        {
            MovieId = id;
        }

        [Required]
        public int MovieId { get; set; }

        [Required]
        [StringLength(5000, MinimumLength = 10, ErrorMessage = "A comment must contain {2} to {1} characters")]
        public string Comment { get; set; }

        [Required]
        [Range(1, 10, ErrorMessage = "A rating must be between 1 and 10 stars")]
        public int Rating { get; set; }
    }
}
