﻿namespace FletNix.ViewModels.Movie_watch
{
    public class WatchMovie
    {
        public WatchMovie() { }

        public WatchMovie(int id)
        {
            WriteComment = new WriteCommentModel(id);
        }

        public WatchMovie(WriteCommentModel writeComment)
        {
            WriteComment = writeComment;
        }

        public WatchMovie(UpdateCommentModel updateComment)
        {
            UpdateComment = updateComment;
        }

        public WriteCommentModel WriteComment { get; set; }
        public UpdateCommentModel UpdateComment { get; set; }
    }
}
