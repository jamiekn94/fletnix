﻿using System.Collections.Generic;

namespace FletNix.ViewModels.Movie_overview
{
    public class SearchMovie
    {
        public SearchMovie()
        {
            Genres = new List<string>();
        }

        public SearchMovie(int category)
        {
            Category = category;
            Genres = new List<string>();
        }

        public int Category { get; set; }

        public decimal? MinPrice { get; set; }

        public decimal? MaxPrice { get; set; }

        public int? Year { get; set; }

        public List<string> Genres { get; set; }
    }
}
