﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Global
{
    public class XFrameHeader : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
          
        }
    }
}
