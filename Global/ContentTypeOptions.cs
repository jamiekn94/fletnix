﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Global
{
    public class ContentTypeOptionsHeader : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.Response.Headers.Add("X-Content-Type-Options", "nosniff");
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
           
        }
    }
}
