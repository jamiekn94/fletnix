﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Global
{
    public class XSSHeader : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.Response.Headers.Add("X-XSS-Protection", "1;mode=block");
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
          
        }
    }
}
