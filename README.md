## Inleiding ##
Voor het vak (NOTS) Web Applications heb ik de opdracht gekregen om een nieuwe video-on-demand internetplatform te realiseren. Hierbij wordt de focus gelegd op performance en security. 

## Doelgroepen ##
De video-on-demand platform heeft de volgende doelgroepen:

* Applicatiebeheerder

De applicatiebeheerder is verantwoordelijk voor het beheren van de films.

* Financiële manager

De financiële manager is geïnteresseerd in de resultaten van films. Hij of zij wilt statistieken kunnen inzien van films zoals awards, ratings.

* CEO

De chief executive officer wilt overzichten kunnen uitprinten van rating rapports. 

* Klant

Het doel van de klant is om zo eenvoudig mogelijk films te kunnen zoeken en te bekijken. 

# Functioneel ontwerp #

## Functionele eisen ##
De opdrachtgever heeft mij meerdere uses cases gegeven waaraan de applicatie moet voldoen. Deze uses cases zijn hieronder per use case uitgewerkt in functionele eisen.

### Use case 1: Beheren van een film ###
Onderhouden van filmgegevens, toevoegen nieuwe items en aanpassen van bestaande films.

* Het systeem moet een overzicht kunnen tonen van films
* Het systeem een film kunnen aanpassen inclusief: cast, genre en directors
* Het systeem moet een film te kunnen toevoegen inclusief: cast, genre en directors
* Het systeem moet een film kunnen verwijderen

### Use case 2: Film kijken ###
Gebruiker moet een film kunnen kijken op basis van gegeven zoekcriteria.

* Het systeem moet een overzicht van films tonen gesorteerd op meest populaire films ooit
* Het systeem moet een overzicht van films tonen gesorteerd op meest populaire films van de afgelopen twee weken
* Het systeem moet de geselecteerde film kunnen afspelen
* Het systeem moet film gegevens kunnen tonen inclusief: cast, genre, directors en opvolgende films van de desbetreffende film
* Het systeem moet de bekeken film toevoegen in de gebruikers afspeellijst
* Het systeem moet zoekopties geven. Deze zoekopties bevatten de volgende opties: titel, jaartal, minimum en maximale prijs

### Use case 3: Beoordelen van een film ###
Gebruiker kan een film beoordelen door het geven van feedback en een waardering.

* Het systeem moet een overzicht van de bekeken films van een klant weergeven
* Het systeem moet een review kunnen toevoegen die bestaat uit een comment en waardering. Een comment moet minimaal uit 10 karakters bestaan en een waardering moet tussen de 1 en 10 liggen.
* Het systeem moet een review kunnen verwijderen
* Het systeem moet een review kunnen wijzigen

### Use case 4: Film nominaties ###
Een applicatiebeheerder moet nominaties kunnen aanpassen van een film.

* Het systeem moet een overzicht van films kunnen tonen
* Het systeem moet de nominaties van de gekozen film kunnen tonen
* Het systeem moet een nominatie kunnen toevoegen aan een film. Een nominatie bestaat uit de volgende keuzes: won en nominated.
* Het systeem moet moet een nominatie kunnen aanpassen
* Het systeem omet een nominatie kunnen verwijderen

### Use case 5: Overzicht awards ###
Weergeven van een overzicht met film awards.

* Het systeem moet een overzich van films inclusief: awards, hoeveelheid gewonnen awards, hoeveelheid nominated awards en de totale hoeveelheid awards
* Het systeem moet kunnen zoeken op een minimale jaartaal
* Het systeem moet kunnen zoeken op een maximale jaartaal

### Use case 6: Overzicht ratings ###
Weergeven van films inclusief ratings

* Het systeem moet top 10 overzicht kunnen tonen van films met de laagste gemiddelde rating
* Het systeem moet top 10 overzicht kunnen tonen van films met de hoogste gemiddelde rating
* Het systeem moet alle films kunnen tonen op basis van een rating-price gesorteerd op hoog van laag. Dit wordt berekend door de gemiddelde rating te delen door de prijs.

## Niet functionele eisen ##
Het platform moet voldoen aan de volgende niet functionele eisen:

* Het platform moet ontwikkeld worden in Microsoft ASP.NET Core MVC 5.0
* Het platform moet draaien op het Microsoft Azure platform
* Het platform moet bestand zijn tegen alle OWASP top 10 kwetsbaarheden
* Het platform moet highly scalable zijn 

## Schermontwerpen ##
Er zijn in total twee schermontwerpen gemaakt voor de overzicht en detail pagina omdat dit de meest gebruikte pagina's zijn.

### Overzicht pagina ###
In de overzichtspagina worden 12 films weergeven op 4 rijen, 3 per rij. De gebruiker heeft links de mogelijkheid om te sorteren en te filteren op films.
Onder de lijst van films heeft de gebruiker de mogelijkheid om te navigeren naar de volgende en vorige pagina.

![Movie list.png](https://bitbucket.org/repo/p44yrog/images/3304502885-Movie%20list.png)

### Detail pagina ###
In de detail pagina heeft de gebruiker de mogelijkheid om een film te kijken en een beoordeling te plaatsen. Daarnaast toont deze pagina alle cast members, andere films in de reeks, genres, directors en comments.

![Details.png](https://bitbucket.org/repo/p44yrog/images/228960716-Details.png)

# Technisch ontwerp #
In het technisch ontwerp wordt alle keuzes vastgelegd rondom techniek, architectuur, security en performance.

## Applicaties ##
Het platform zal bestaan uit meerdere web applicaties. Het platform bestaat uit de volgende applicaties:

### Autorisatie webapplicatie ###

In deze app kunnen gebruikers zich aanmelden en registreren. Alle klanten, applicatiebeheerders en andere gebruikers van het platform zullen deze applicatie hiervoor gebruiken.

### Klanten webapplicatie ###

In deze app kunnen klanten films zoeken en bekijken. 

### Administratie webapplicatie ###
In deze applicatie kunnen beheerders, financiële managers en CEO's administratieve functies uitvoeren.

## Architectuur ##
De opdrachtgever wilt dat de applicatie gerealiseerd gaat worden in ASP.NET Core MVC 5 in de in de programmeertaal C#. Om data persistent te kunnen opslaan zal gebruik gemaakt worden van de relationele database MSSQL. De applicatie zal highly scalable moeten zijn. Dit houdt in dat de web applicatie zo gebouwd moet worden dat meerdere web applicaties in een load balancer kunnen draaien zonder session persistence kwijt te raken.

## Hosting ##
Het gehele platform zal gehost worden op het Azure platform van Microsoft. Dit houdt in: meerdere web applicaties, databases en een load balancer. 

## Techniek ##
De applicatie maakt gebruik van een aantal moderne technieken en tools. Deze zijn hieronder beschreven. 

### Identity Server ###
Om verschillende applicaties aan een enkele authenticatie te koppelen wordt Identity Server 4 gebruikt. In Identity Server kan geconfigureerd worden welke gegevens zichtbaar mogen zijn voor een gebruiker. Er kan dus onderscheid gemaakt worden tussen administrator en gebruiker. Zo kan een administrator wel inloggen op de administratie website, maar een normale gebruiker niet. Dit wordt gedaan door middel van claims. Wanneer een gebruiker inlogt kan gevraagd worden om een bepaalde claim. Deze claim bepaald welke assets of te wel data de gebruiker recht op heeft om in te zien.

### Entity Framework ###
Om te kunnen communiceren met de MSSQL database wordt het Entity Framework gebruikt. Het Entity Framework is een object-relational mapper waarin developers query's kunnen uitvoeren op objecten door middel van LINQ. Hierdoor hoeven handmatig geen SQL query's meer te schrijven/ Door gebruik te maken van Entity framework kan type safety gegarandeerd worden waardoor de developer op de hoogte gebracht wordt wanneer een niet bestaande property of verkeerde type meegegeven wordt.  

### Dependency Injection ###
Het ASP.NET Core framework maakt standaard gebruik van dependency injection. Hierdoor worden klassen relaties in code voorkomen. Bij het configureren van dependency injection geef je de interface en de implementatie die hoort bij de interface. Zodra je de implementatie van de interface wilt gebruiken zal deze meegegeven worden in de constructor. Hierdoor wordt tighly coupling voorkomen in de code.

### Memory caching ###
Om langdurige database query's te voorkomen zullen op verschillende plekken in memory caching gebruikt worden. Op moment is nog niet duidelijk waar deze functionaliteit geimplementeerd zal worden. Dit zal doormiddel van performance profiling onderzocht worden.

### MVC ###
Om HTML en logica van elkaar te scheiden is er voor gekozen om het MVC pattern te gebruiken. De controllers bevatten de logica van de applicatie. De controllers verwerken het resultaat en geven dit vervolgens mee aan views doormiddel van models. Models bevatten properties en validatie van data. Wanneer een request gemaakt wordt en de actie een model bevat als parameter zal deze model gevuld worden met data van de server die de gebruiker meestuurt. In de controller wordt vervolgens gegeken of de model valide is. 

## Ontwerpbeslissingen ##
Hieronder zijn meerdere ontwerpbeslissingen beschreven die ik tijdens de realisatie heb gemaakt.

### Repository pattern ###
Voor de database laag is een repository pattern gebruikt. Elk repository is gekoppeld aan een tabel. Elke repository is vervolgens toegevoegd aan een unit of work. Deze unit of work bevat alle repositories. Al deze repositories maken gebruik van een enkele database context waardoor meerdere wijzigingen met een enkele save opgeslagen verwerkt kunnen worden in de database. Uiteindelijk bleek de context niet thread safe te zijn waardoor voor elke repo alsnog een nieuwe context gemaakt moest worden. Hierdoor kunnen wijzigingen in verschillende repositories niet met een enkele save opgeslagen worden.

### Async & Await ###
Om gebruik te maken van multi threading is async en await geïmplementeerd voor netwerk operaties. Hierdoor kunnen meerdere operaties die onafhankelijk zijn van elkaar parallel uitgevoerd worden. Hierdoor zal de performance verbetered worden en zal de website meer gebruikers kunnen ondersteunen.

### Eager loading ###
Wanneer zeker is dat database relaties opgehaald moeten worden, zal gebruik gemaakt worden van eager loading. Dit houdt in dat relaties gelijk opgehaald worden zonder dat een relatie aangeroepen hoeft te worden in code. Het entity framework is standaard lazy loading waardoor het relaties alleen ophaalt wanneer je een relatie aanroept in code. Dit betekend dat er round trips gemaakt worden naar de database wat extra tijd in beslag neemt. Dit is wat lazy loading voorkomt door gelijk alle relaties op te halen.

### Pagination ###
Omdat de database veel films, beoordelingen en cast members kan bevatten is er voor gekozen om pagination te implementeren. Dit biedt overzicht voor de gebruikers en anderen die de applicaties gaan gebruiken. Daarnaast zal het de performance verbeteren omdat duizenden resultaten ophalen en laten zien in de browser veel tijd in beslag kan nemen.

### Populaire films ophalen ###
Tijdens de realisatie viel op dat populaire films ophalen veel tijd in beslag neemt. Dit komt omdat het Entity framework het niet voor elkaar krijgt om query's te genereren waarbij de volledige query in de database wordt uitgevoerd. Dit resulteerde in dat alle resultaten vanuit de database worden opgehaald en vervolgens in memory worden verwerkt. Om dit op te lossen is handmatig een SQL query geschreven waardoor niet alle resultaten vanuit de database worden opgehaald. 

### Personen zoekfunctie ###
Bij het toevoegen van een cast member is een extra pagina toegevoegd. In deze pagina kan gezocht worden naar een persoon. Hiervoor is gekozen omdat de database duizenden personen bevat. Door deze zoek functionaliteit te implementeren wordt de lijst met personen waar de applicatiebeheerder uit kan kiezen vele malen kleiner. 

### Cached movie detail pagina ###
De detail pagina maakt veel database query's om bijvoorbeld de; filmreeks, comments, directors en cast members op te halen. Om dit proces sneller te laten voorlopen is in memory caching gebruikt. Al deze resultaten behalve de comments worden voor 15 minuten opgeslagen in geheugen. 

## Load testing ##
De volgende load tests zijn geschreven om de meest gebruikte pagina's te controleren op performance. 

### Case 1: Async & Await ###
In deze use case wordt de populaire films van de afgelopen twee pagina getest. Hierbij is de eerste test geschreven met synchrone en de tweede met asynchrone code.

Doel: Kan ik meer requests aan, worden requests sneller geladen, hoeveel gebruikers kan deze pagina aan?

#### Sync  ####

* Minimum response time: 0.079 seconden
* Average response time: 0.79 seconden
* Maximum response time: 4.59 seconden
* Maximaal 265 gebruikers, gaat daarna errors produceren

![Sync.png](https://bitbucket.org/repo/p44yrog/images/3498373389-Sync.png)

#### Async (Parallel) ####

* Minimum response time: 0.081 seconden
* Average response time: 2.13 seconden
* Maximum response time: 5.10 seconden
* Maximaal 745 gebruikers, gaat daarna errors produceren

![Async.png](https://bitbucket.org/repo/p44yrog/images/1130991039-Async.png)

Conclusie: Pagina's worden niet sneller geladen maar de applicatie kan wel meer requests aan.

###  Case 2: Memory caching ###

In deze case wordt de detail pagina van een film getest. De eerste test in deze case maakt geen gebruik van caching. De tweede test maakt wel gebruik van caching.

Doel: Wordt door caching de requests sneller geladen, hoeveel meer requests kan ik nu aan, hoeveel gebruikers kan deze pagina aan?

#### Non cache ####

* Minimum response time: 0.78 seconden
* Average response time: 2.81 seconden
* Maximum response time: 4.76 seconden
* Maximaal 115 gebruikers, gaat daarna errors produceren

![Non caching.png](https://bitbucket.org/repo/p44yrog/images/1531978974-Non%20caching.png)

#### Cache ####

* Minimum response time: 0.14 seconden
* Average response time: 2.61 seconden
* Maximum response time: 5.19 seconden
* Maximaal 685 gebruikers, gaat daarna errors produceren

![Cached.png](https://bitbucket.org/repo/p44yrog/images/700925264-Cached.png)

Conclusie: Caching zorgt ervoor dat de pagina sneller laad en vele malen meer gebruikers aan kan.

# Security #
De opdrachtgever heeft aangegeven beschermd te willen zijn tegen de top 10 OWASP applicatie risico's. De oplossing van deze risco's zijn hieronder opgenomen. 

## Injection ##
Het Entity Framework maakt gebruik van parameterized queries. Dit houdt in dat de query en parameters los van elkaar verzonden worden naar de database. De parameters worden hierbij alleen gebruikt als values. Hierdoor kan de SQL query niet gemanipuleerd worden. OS Injection is niet mogelijk omdat er geen mogelijkheid is om code toe te voegen aan de website zoals bijvoorbeeld een upload systeem. 

## Broken Authentication and Session Management ##
De webapplicatie is beveiligd door SSL. Authentication cookies van ASP.NET Identity zijn zo ingesteld dat deze alleen verzonden worden over een SSL verbinding en JavaScript geen toegang heeft tot de authenticatie cookie. De Identity 4 authenticatie cookie kan wel via JavaScript benaderd worden. Dit zat hardcoded in de broncode van Identity Server 4. De lifetime van een authenticatie cookie bevat een sliding expiration time van 20 minuten. Na deze tijd kan de cookie niet meer gebruikt worden om in te loggen. Wachtwoorden in de applicatie zijn gehashed waardoor niet te achterhalen is wat het wachtwoord is. Verder worden session id's niet blootgesteld in de URL.

## Cross-Site Scripting (XSS) ##
De razor engine in MVC encode standaard alle data vanuit de server. Hierdoor wordt HTML en JavaScript als plaintext weergegeven. Hierdoor is XSS injection niet mogelijk.

## Broken Access Control ##
Alle admin pagina's behalve de homepagina is beschermd door middel van Identity Claims. Een identity claim, in ons geval Role bevat de rol van een gebruiker. Alleen wanneer de gebruiker de rol als "Administrator" heeft kan hij gebruik maken van administratie functionaliteiten. In elke controller is de "Administrator" rol opgenomen waardoor alle actions in deze controller beschermd zijn tegen niet administratoren. Verder heeft de applicatie geen URL's waarin het mogelijk is om andere waarde op te geven waardoor hij gegevens kan in zien waar hij/zij geen recht tot heeft.

## Security Misconfiguration ##
Alle software tot 14 Juni 2017 is up to date. De database van Azure is beschermd door een moeilijk wachtwoord en bevat een Firewall waardoor alleen geen geauthoriseerde gebruikers toegang hebben tot de database. Configuratiebestanden zijn niet niet te benaderen vanuit buitenaf en bevatten geen gevoelige data. De applicatie is zo ingesteld dat er geen stacktraces of andere gevoelige error gegevens worden getoond wanneer de applicatie op Azure draait.  

## Sensitive Data Exposure ##
De applicatie bevat geen gevoelige data zoals backups die van buitenaf te benaderen zijn. Voor wachtwoorden wordt een sterk algorithme gebruikt (PBKDF2 with HMAC-SHA1, 1.000 interaties).

## Insufficient Attack Protection ##
Web applicatie is getest met OWASP ZAP 2.6.0. De enige risico's zijn: Identity server 4 cookies zijn niet HttpOnly. Dit kan niet opgelost worden omdat het hardcoded in Identity Server staat. Daarnaast raad het aan om auto complete uit te zetten voor wachtwoord velden. Er is voor gekozen om auto complete omdat gebruikers deze functionaliteit als handig kunnen beschouwen. 

## Cross-Site Request Forgery (CSRF) ##
ASP.NET Core bevat standaard functionaliteiten om de applicatie te beschermen tegen dit soort aanvallen. Bij het maken van een form in HTML wordt standaard een random verificatie code gegenereerd. Deze code wordt als hidden input meegegeven in het form en wordt ook meeverzonden naar de server. Deze verificatie code wordt ook meegestuurd in een cookie. Op de server wordt vervolgens gecontroleerd of de cookie value en de waarde uit het form hetzelfde zijn. Zoniet, dan wordt het proces afgebroken.

## Using Components with Known Vulnerabilities ##
Versies van server packages en client side packages worden bijgehouden. Voor server side packages wordt Nuget gebruikt. Deze houdt bij wat er geinstalleerd is en welke versie. Deze packages kunnen eenvoudig geüpdate worden in tools zoals Visual Studio. Voor client side packages wordt Bower gebruikt. Bower houd bij wat geinstalleerd en hierbij de versies. Ook kan via Visual Studio client side packages via Bower packages geüpdate worden.

## Underprotected APIs ##
De webapplicatie stelt geen API's beschikbaar waardoor deze niet vatbaar is voor dit risico.

## Zapp Report ##
![vulnerabilities.png](https://bitbucket.org/repo/p44yrog/images/504488492-vulnerabilities.png)